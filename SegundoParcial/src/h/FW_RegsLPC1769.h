/** ==============================================
 *
 * @file		FW_RegsLPC1769.h
 *
 *
 * @brief	
 *
 * @author		Mariano Koremblum,	marianotec7@gmail.com
 *
 * @date 		18/11/2013
 *
 * @version
 *
	============================================== */

#ifndef FW_REGSLPC1769_H_
#define FW_REGSLPC1769_H_

/**************************************************************************/

//										INCLUDES								

/**************************************************************************/

#include <stdio.h>

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//										DEFINES								

/**************************************************************************/

#define		PORT0		0
#define		PORT1		1
#define		PORT2		2
#define		PORT3		3
#define		PORT4		4

#define		FUNCION_GPIO	0
#define		FUNCION_1		1
#define		FUNCION_2		2
#define		FUNCION_3		3

#define		MODO_0		0
#define		MODO_1		1
#define		MODO_2		2
#define		MODO_3		3

#define		ENTRADA		0
#define		SALIDA		1

#define		__R					volatile const
#define		__W					volatile
#define		__RW				volatile

//!< GPIO - PORT0
/*	*						*
 *************************
 *		FIODIR			*	0x2009C000
 *************************
 *		RESERVED		*	0x2009C004
 *************************
 *		RESERVED		*	0x2009C008
 *************************
 *		RESERVED		*	0x2009C00C
 *************************
 *		FIOMASK			*	0x2009C010
 *************************
 *		FIOPIN			*	0x2009C014
 *************************
 *		FIOSET			*	0x2009C018
 *************************
 *		FIOCLR			*	0x2009C01C
 *************************
 *						*
 *						*
 */
#define		GPIO		( ( __RW uint32_t * ) 0x2009C000UL )

//!< ////////////////Registros PINSEL//////////////////////////////
//!< 00	GPIO (reset value)		01	funcion 1
//!< 11	funcion 3				10	funcion 2

//!< //////////////////Registros PINMODE ///////////////////////////
#define		PINMODE		( ( __RW uint32_t * ) 0x4002C040UL )

//!< ----------- Estados de PINMODE
//!< 00	Pull Up resistor enable (reset value)		01	repeated mode enable
//!< 11	Pull Down resistor enable					10	ni Pull Up ni Pull DOwn
#define		PINMODE_PULLUP 		0
#define		PINMODE_REPEAT 		1
#define		PINMODE_NONE 		2
#define		PINMODE_PULLDOWN 	3


//0xE000E100UL : Direccion de inicio de los registros de habilitación (set) de interrupciones en el NVIC:

#define		ISER		( ( uint32_t * ) 0xE000E100UL )
//0xE000E180UL : Direccion de inicio de los registros de deshabilitacion (clear) de interrupciones en el NVIC:
#define		ICER		( (  uint32_t * ) 0xE000E180UL )


//Registros ISER: Para habilitar las Interupciones Se activan con 1 Escribiendo un 0 no hace nada

#define		ISER0		ISER[0]
#define		ISER1		ISER[1]
//	#define		ISE_EINT3	ISER[0] |= (0x00000001 << 21)  //ISER0->bit21 pongo un 1 en el bit 21 para habilitar la INT EINT3
//  #define     ISE_EINT2	ISER[0] |= (0x00000001 << 20)  //ISER0->bit20 pongo un 1 en el bit 20 para habilitar la INT EINT2
//  #define     ISE_EINT1	ISER[0] |= (0x00000001 << 19)  //ISER0->bit19 pongo un 1 en el bit 19 para habilitar la INT EINT1
//  #define     ISE_EINT0	ISER[0] |= (0x00000001 << 18)  //ISER0->bit18 pongo un 1 en el bit 18 para habilitar la INT EINT1

// Registro EXTMODE : Para seleccionar si la ISR Externa activa por flanco ó nivel
#define		EXTMODE 		( (uint32_t  * ) 0x400FC148 )[0]
//	#define		EXTMODE3_F		EXTMODE[0] |= 0x00000001 << 3  // EINT3 por flanco
//  #define		EXTMODE2_F		EXTMODE[0] |= 0x00000001 << 2  // EINT2 por flanco
//  #define		EXTMODE1_F		EXTMODE[0] |= 0x00000001 << 1  // EINT1 por flanco
//  #define		EXTMODE0_F		EXTMODE[0] |= 0x00000001       // EINT0 por flanco

// Registro EXTPOLAR : selecciona Polaridad del EXTMODE
#define    EXTPOLAR        ( (uint32_t  * ) 0x400FC14C )[0]
//  #define    EXTPOLAR3_P      EXTPOLAR[0] |= 0X00000001 << 3 // Flanco ó Nivel Positivo
//  #define    EXTPOLAR2_P      EXTPOLAR[0] |= 0X00000001 << 2 // Flanco ó Nivel Positivo
//  #define    EXTPOLAR1_P      EXTPOLAR[0] |= 0X00000001 << 1 // Flanco ó Nivel Positivo
//  #define    EXTPOLAR0_P      EXTPOLAR[0] |= 0X00000001      // Flanco ó Nivel Positivo


//Registros ICER: Para deshabilitar las Interupciones Se desactivan con 1 Escribiendo un 0 no hace nada
//Registros ICER:

#define		ICER0		ICER[0]
#define		ICER1		ICER[1]
//	#define		ICE_EINT3	ICER0 |= (0x00000001 << 21) // deshabilito a EINT3
//  #define		ICE_EINT2	ICER0 |= (0x00000001 << 20) // deshabilito a EINT2
//  #define		ICE_EINT1	ICER0 |= (0x00000001 << 19) // deshabilito a EINT1
//  #define		ICE_EINT0	ICER0 |= (0x00000001 << 18) // deshabilito a EINT0


#define		EXTINT 		( (uint32_t  * ) 0x400FC140UL )[0] // Reg de Flags para limpiar la ISR

//	#define		CLR_EINT3		EXTINT[0] |= 0x00000001 << 3 // bajo el flag de EINT3
//  #define		CLR_EINT2		EXTINT[0] |= 0x00000001 << 2 // bajo el flag de EINT2
//  #define		CLR_EINT1		EXTINT[0] |= 0x00000001 << 1 // bajo el flag de EINT1
//  #define		CLR_EINT0		EXTINT[0] |= 0x00000001      // bajo el flag de EINT0



//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Registros de GPIO para usarse como ISR
// Configuración
#define      IO0IntEnR  ( (uint32_t  * ) 0x40028090UL ) // Puerto 0 como flanco ascendente
#define      IO2IntEnR  ( (uint32_t  * ) 0x400280B0UL )//  Puerto 2 como flanco ascendente
#define      IO0IntEnF  ( (uint32_t  * ) 0x40028094UL )// Puerto 0 como flanco descendente
#define      IO2IntEnF  ( (uint32_t  * ) 0x400280B4UL )// Puerto 2 como flanco ascendente

// Estado
#define     IO0IntStatR  ( (uint32_t  * ) 0x40028084UL ) //Estado de los flags de interr flanco ascendente bits Puerto 0
#define     IO2IntStatR  ( (uint32_t  * ) 0x400280A4UL ) //Estado de los flags de interr flanco ascendente bits Puerto 2
#define     IO0IntStatF  ( (uint32_t  * ) 0x40028088UL ) //Estado de los flags de interr flanco descendente bits Puerto 0
#define     IO2IntStatF  ( (uint32_t  * ) 0x400280A8UL ) //Estado de los flags de interr flanco descendente bits Puerto 2
#define     IOIntStatus ( (uint32_t  * ) 0x40028080UL ) //Estado de los flags de interr de bits Puerto 2 y Puerto 0

//Bajo flags de Interr por GPIO
#define     IO0IntClr  ( (uint32_t  * ) 0x4002808CUL ) //Bajo flags de Interr Puerto 0
#define     IO2IntClr  ( (uint32_t  * ) 0x400280ACUL ) //Bajo flags de Interr Puerto 2

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#define EINT0  0
#define EINT1  1
#define EINT2  2
#define EINT3  3

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//										MACROS								

/**************************************************************************/



/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//										DATA TYPES								

/**************************************************************************/


typedef 	unsigned int 		uint32_t;
typedef 	unsigned short 		uint16_t;
typedef 	unsigned char 		uint8_t;

typedef enum {FALSE = 0, TRUE = !FALSE} bool;

/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//								   GLOBAL VARIABLES								

/**************************************************************************/



/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//								  FUNCTIONS	DECLARATIONS

/**************************************************************************/

/**************************************************************************/
/**************************************************************************/

#endif /* FW_REGSLPC1769_H_ */
