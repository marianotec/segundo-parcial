/** ==============================================
 *
 * @file		FW_Infotronic.h
 *
 *
 * @brief	
 *
 * @author		Mariano Koremblum,	marianotec7@gmail.com
 *
 * @date 		18/11/2013
 *
 * @version
 *
	============================================== */

#ifndef FW_INFOTRONIC_H_
#define FW_INFOTRONIC_H_

/**************************************************************************/

//										INCLUDES								

/**************************************************************************/



/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//										DEFINES								

/**************************************************************************/

#define		ENTRADA			0
#define		SALIDA			1

//	Identificación de los puertos de expansion:

#define EXPANSION0			PORT2,7
#define EXPANSION1			PORT1,29
#define EXPANSION2			PORT4,28
#define EXPANSION3			PORT1,23
#define EXPANSION4			PORT1,20
#define EXPANSION5			PORT0,19
#define EXPANSION6			PORT3,26
#define EXPANSION7			PORT1,25
#define EXPANSION8			PORT1,22
#define EXPANSION9			PORT1,19
#define EXPANSION10			PORT0,20
#define EXPANSION11			PORT3,25
#define EXPANSION12			PORT1,27
#define EXPANSION13			PORT1,24
#define EXPANSION14			PORT1,21
#define EXPANSION15			PORT1,18
#define EXPANSION16			PORT1,31
#define EXPANSION17			PORT0,24
#define EXPANSION18			PORT0,25
#define EXPANSION19			PORT0,17
#define EXPANSION20			PORT1,31
#define EXPANSION21			PORT0,22
#define EXPANSION22			PORT0,15
#define EXPANSION23			PORT0,16
#define EXPANSION24			PORT2,8
#define EXPANSION25			PORT2,12
#define EXPANSION26			PORT1,31
#define EXPANSION27			PORT1,31

//********************* ENTRADAS DIGITALES **********************//

#define 	ED0				PORT1,26
#define 	ED1				PORT4,29
#define 	ED2				PORT2,11

//********************** SALIDAS DIGITALES **********************//

//-------------- Relays --------------///
#define		RELAY1			PORT2,0
#define		RELAY2			PORT0,23
#define		RELAY3			PORT0,21
#define		RELAY4			PORT0,27

#define		RELAY1_ON		SetPIN(RELAY1, ON)
#define		RELAY2_ON		SetPIN(RELAY1, ON)
#define		RELAY3_ON		SetPIN(RELAY1, ON)
#define		RELAY4_ON		SetPIN(RELAY1, ON)

#define		RELAY1_OFF		SetPIN(RELAY1, OFF)
#define		RELAY2_OFF		SetPIN(RELAY1, OFF)
#define		RELAY3_OFF		SetPIN(RELAY1, OFF)
#define		RELAY4_OFF		SetPIN(RELAY1, OFF)


//-------------- Leds --------------///

#define		LED1			RELAY1
#define		LED2			RELAY2
#define		LED3			RELAY3
#define		LED4			RELAY4

#define		LED1_ON			RELAY1_ON
#define		LED2_ON			RELAY2_ON
#define		LED3_ON			RELAY3_ON
#define		LED4_ON			RELAY4_ON

#define		LED1_OFF		RELAY1_OFF
#define		LED2_OFF		RELAY2_OFF
#define		LED3_OFF		RELAY3_OFF
#define		LED4_OFF		RELAY4_OFF

//-------------- Buzzer --------------///

#define		BUZZ			PORT0,28

#define		BUZZ_ON			SetPIN(BUZZ, ON)
#define		BUZZ_OFF		SetPIN(BUZZ, OFF)

//-------------- RBG --------------//

#define		RGBR			PORT2,3
#define		RGBG			PORT2,2
#define		RGBB			PORT2,1

#define		RED_ON			SetPIN(RGBR, ON)
#define		GREEN_ON		SetPIN(RGBG, ON)
#define		BLUE_ON			SetPIN(RGBB, ON)

#define		RED_OFF			SetPIN(RGBR, OFF)
#define		GREEN_OFF		SetPIN(RGBG, OFF)
#define		BLUE_OFF		SetPIN(RGBB, OFF)

#define		RGB_OFF			RED_OFF;GREEN_OFF;BLUE_OFF
#define		RGB_ON			RED_ON;GREEN_ON;BLUE_ON

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//								MACROS

/**************************************************************************/



/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//								DATA TYPES

/**************************************************************************/



/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//						   GLOBAL VARIABLES

/**************************************************************************/



/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//						  FUNCTIONS	DECLARATIONS

/**************************************************************************/

void SetPINSEL ( uint8_t  , uint8_t  ,uint8_t );
void SetPINMODE( uint8_t  , uint8_t  ,uint8_t );
void SetDIR( uint8_t  , uint8_t  , uint8_t  );
void SetPIN( uint8_t  , uint8_t  , uint8_t  );
uint8_t GetPIN( uint8_t  , uint8_t  , uint8_t );

void Inicializar_RGB( void );
void Inicializar_Relays( void );
void Inicializar_Display( void );
void Inicializar_Buzzer( void );
void Inicializar_EDx(void);

/**************************************************************************/
/**************************************************************************/

#endif /* FW_INFOTRONIC_H_ */
