/** ==============================================
 *
 * @file		SegundoParcial.h
 *
 *
 * @brief	
 *
 * @author		Mariano Koremblum,	marianotec7@gmail.com
 *
 * @date 		18/11/2013
 *
 * @version
 *
	============================================== */

#ifndef SEGUNDOPARCIAL_H_
#define SEGUNDOPARCIAL_H_

/**************************************************************************/

//								INCLUDES

/**************************************************************************/

#include "string.h"
#include "Oscilador.h"
#include "FW_RegsLPC1769.h"
#include "FW_ADC.h"
#include "FW_Systick.h"
#include "FW_PCLKSEL.h"
#include "FW_PCONP.h"
#include "FW_PINSEL.h"
#include "FW_UART.h"
#include "FW_Display.h"
#include "FW_Infotronic.h"
#include "FW_Timers.h"
#include "FW_Teclado_Exp3.h"

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//								DEFINES

/**************************************************************************/

#define		__R					volatile const
#define		__W					volatile
#define		__RW				volatile

#define		NO_KEY				0xff

#define		ACTIVO_BAJO			0
#define		ACTIVO_ALTO			1

#define		OFF					0
#define		ON					1

#define SET_RELAY0_OFF			BufferRelays[0]=0;
#define SET_RELAY1_OFF			BufferRelays[1]=0;
#define SET_RELAY2_OFF			BufferRelays[2]=0;
#define SET_RELAY3_OFF			BufferRelays[3]=0;

#define SET_RELAY0_ON			BufferRelays[0]=1;
#define SET_RELAY1_ON			BufferRelays[1]=1;
#define SET_RELAY2_ON			BufferRelays[2]=1;
#define SET_RELAY3_ON			BufferRelays[3]=1;

#define INICIO					0
#define ESTADO1					1
#define ESTADO2					2
#define ESTADO3					3
#define ESTADO4					4
#define SAMPLE					5
#define DEFAULT					99

#define ROJO					1
#define AZUL					2
#define VERDE					3


#define SET_ROJO_OFF			BufferRGB[0]=0
#define SET_ROJO_ON				BufferRGB[0]=1
#define SET_VERDE_OFF			BufferRGB[1]=0
#define SET_VERDE_ON			BufferRGB[1]=1
#define SET_AZUL_OFF			BufferRGB[2]=0
#define SET_AZUL_ON				BufferRGB[2]=1

#define T0						5
#define T1						25
#define T2						10
#define T3						10
#define T4						25
#define T5						10

#define CALENTAR				1
#define NO_CALENTAR				2

#define PENDIENTE				(4096/310)

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//								MACROS

/**************************************************************************/


/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//							DATA TYPES

/**************************************************************************/

typedef 	unsigned int 		uint32_t;
typedef 	unsigned short 		uint16_t;
typedef 	unsigned char 		uint8_t;

/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//						  GLOBAL VARIABLES

/**************************************************************************/

extern volatile uint32_t ticks;

extern volatile bool MaquinaOn;

extern volatile bool EmergencyButton;

extern volatile bool BufferRGB[];

extern volatile uint8_t BufferRelays[];

extern volatile short Bufferkey;

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//						FUNCTIONS	DECLARATIONS

/**************************************************************************/

void Init(void);

void Buffers_Inicializacion(void);

void EINT3_Inicializacion(void);

void IO_Inicializacion(void);

void Control (void);
	void Lazo_Control (void);
	uint8_t Lazo_inicio (void);
	uint8_t Estado_Uno (void);
	uint8_t Estado_Dos (void);
	uint8_t Estado_Tres (void);
	uint8_t Estado_Cuatro(void);
	uint8_t Sample (void);


void Semaforo (void);
	uint8_t Sem_Rojo(void);
		void parpadeo (void);
	uint8_t Sem_Azul(void);
	uint8_t Sem_Verde(void);

void Calefaccion (void);
	uint8_t Calentar_Off(void);
	uint8_t Calentar(void);

float Temperatura (uint16_t cuentas);

void EnviarTemp (void);

void ShowTemperature (void);

void ShowSetPoint (void);

void ConfigSetPoint (void);

void CheckStart (void);

void SendTemperature(void);

void RGB_Refresh(void);

void Relays(void);

/**************************************************************************/
/**************************************************************************/

#endif /* SEGUNDOPARCIAL_H_ */
