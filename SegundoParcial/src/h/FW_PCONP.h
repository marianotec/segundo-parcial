/** ==============================================
 *
 * @file		FW_PCONP.h
 *
 *
 * @brief
 *
 * @author		Mariano Koremblum,	marianotec7@gmail.com
 *
 * @date 		11/11/2013
 *
 	============================================== */

#ifndef PCONP_H_
#define PCONP_H_

#include "FW_RegsLPC1769.h"

//!< ///////////////////   PCONP   //////////////////////////
//!<  Power Control for Peripherals register (PCONP - 0x400F C0C4) [pag. 62 user manual LPC1769]
//!< 0x400FC0C4UL : Direccion de inicio del registro de habilitación de dispositivos:
#define PCONP		((pconp_t *)	0x400FC0C4UL)	/* Registro de Control de Alimentación de Periféricos */


typedef struct
{
	__RW uint32_t RESERVED0:1;
	__RW uint32_t PCTIM0:1;
	__RW uint32_t PCTIM1:1;
	__RW uint32_t PCUART0:1;
	__RW uint32_t PCUART1:1;
	__RW uint32_t RESERVED1:1;
	__RW uint32_t PCPWM1:1;
	__RW uint32_t PCI2C0:1;
	__RW uint32_t PCSPI:1;
	__RW uint32_t PCRTC:1;
	__RW uint32_t PCSSP1:1;//bit 10
	__RW uint32_t RESERVED2:1;
	__RW uint32_t PCADC:1; //<----------ADC
	__RW uint32_t PCCAN1:1;
	__RW uint32_t PCCAN2:1;
	__RW uint32_t PCGPIO:1;
	__RW uint32_t PCRIT:1;
	__RW uint32_t PCMCPWM:1;
	__RW uint32_t PCQEI:1;
	__RW uint32_t PCI2C1:1;
	__RW uint32_t RESERVED3:1;//bit 20
	__RW uint32_t PCSSP0:1;
	__RW uint32_t PCTIM2:1;
	__RW uint32_t PCTIM3:1;
	__RW uint32_t PCUART2:1;
	__RW uint32_t PCUART3:1;
	__RW uint32_t PCI2C2:1;
	__RW uint32_t PCI2S:1;
	__RW uint32_t RESERVED4:1;
	__RW uint32_t PCGPDMA:1;
	__RW uint32_t PCENET:1;//bit 30
	__RW uint32_t PCUSB:1;

} pconp_t;


#endif /* PCONP_H_ */
