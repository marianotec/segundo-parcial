/** ==============================================
 *
 * @file		FW_Display.h
 *
 *
 * @brief	
 *
 * @author		Mariano Koremblum,	marianotec7@gmail.com
 *
 * @date 		18/11/2013
 *
 * @version
 *
	============================================== */

#ifndef FW_DISPLAY_H_
#define FW_DISPLAY_H_

/**************************************************************************/

//								INCLUDES

/**************************************************************************/



/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//								DEFINES

/**************************************************************************/

#define 	BEGIN				(index==0)

#define 	BIT0				EXPANSION0
#define 	BIT1				EXPANSION1
#define 	BIT2				EXPANSION2
#define		BIT3				EXPANSION3

#define		DOT					EXPANSION4

#define 	ONE_SECOND			((ticks%1000)==0) 	//Esto se cumple para 1 tick cada 1ms

#define		ARBITRARY_VALUE		111					//VALOR ARBITRARIO PARA QUE COMIENCE APAGADO EL 7SEGMENTOS
#define 	SET_DIGIT_OFF		ARBITRARY_VALUE

#define		DIGIT_QUANTITY		6					//CANTIDAD DE DIGITOS
#define 	DIGIT_QUANTITY_FIX	DIGIT_QUANTITY

#define 	DIGITOS 			DIGIT_QUANTITY

#define		DIGITS_FORMAT		""			//FORMATO DE LA STRING DE DIGITOS PARA MOSTRAR TEMPERATURA

#define 	DISPLAY_LEFT		0
#define 	DISPLAY_RIGHT		1

//Estas macros son para prender y apagar el punto del 7 segmentos
#define 	DIGIT_DOT_ON		SetPIN(DOT, ACTIVO_ALTO)
#define 	DIGIT_DOT_OFF		SetPIN(DOT, ACTIVO_BAJO)

#define 	DIGIT_VALUE0		SetPIN(BIT0,ACTIVO_BAJO);SetPIN(BIT1,ACTIVO_BAJO);SetPIN(BIT2,ACTIVO_BAJO);SetPIN(BIT3,ACTIVO_BAJO)
#define 	DIGIT_VALUE1		SetPIN(BIT0,ACTIVO_ALTO);SetPIN(BIT1,ACTIVO_BAJO);SetPIN(BIT2,ACTIVO_BAJO);SetPIN(BIT3,ACTIVO_BAJO)
#define 	DIGIT_VALUE2		SetPIN(BIT0,ACTIVO_BAJO);SetPIN(BIT1,ACTIVO_ALTO);SetPIN(BIT2,ACTIVO_BAJO);SetPIN(BIT3,ACTIVO_BAJO)
#define		DIGIT_VALUE3		SetPIN(BIT0,ACTIVO_ALTO);SetPIN(BIT1,ACTIVO_ALTO);SetPIN(BIT2,ACTIVO_BAJO);SetPIN(BIT3,ACTIVO_BAJO)
#define		DIGIT_VALUE4		SetPIN(BIT0,ACTIVO_BAJO);SetPIN(BIT1,ACTIVO_BAJO);SetPIN(BIT2,ACTIVO_ALTO);SetPIN(BIT3,ACTIVO_BAJO)
#define		DIGIT_VALUE5		SetPIN(BIT0,ACTIVO_ALTO);SetPIN(BIT1,ACTIVO_BAJO);SetPIN(BIT2,ACTIVO_ALTO);SetPIN(BIT3,ACTIVO_BAJO)
#define 	DIGIT_VALUE6		SetPIN(BIT0,ACTIVO_BAJO);SetPIN(BIT1,ACTIVO_ALTO);SetPIN(BIT2,ACTIVO_ALTO);SetPIN(BIT3,ACTIVO_BAJO)
#define 	DIGIT_VALUE7		SetPIN(BIT0,ACTIVO_ALTO);SetPIN(BIT1,ACTIVO_ALTO);SetPIN(BIT2,ACTIVO_ALTO);SetPIN(BIT3,ACTIVO_BAJO)
#define 	DIGIT_VALUE8		SetPIN(BIT0,ACTIVO_BAJO);SetPIN(BIT1,ACTIVO_BAJO);SetPIN(BIT2,ACTIVO_BAJO);SetPIN(BIT3,ACTIVO_ALTO)
#define 	DIGIT_VALUE9		SetPIN(BIT0,ACTIVO_ALTO);SetPIN(BIT1,ACTIVO_BAJO);SetPIN(BIT2,ACTIVO_BAJO);SetPIN(BIT3,ACTIVO_ALTO)
#define 	DIGIT_NOTVALUE  	SetPIN(BIT0,ACTIVO_ALTO);SetPIN(BIT1,ACTIVO_ALTO);SetPIN(BIT2,ACTIVO_ALTO);SetPIN(BIT3,ACTIVO_ALTO)

#define 	CLK_MUX				EXPANSION5
#define 	RST_MUX				EXPANSION6

#define 	MUX_7SEG			SetPIN(CLK_MUX,ACTIVO_ALTO);SetPIN(CLK_MUX,ACTIVO_BAJO)
#define		RST_7SEG			SetPIN(RST_MUX,ACTIVO_ALTO);SetPIN(RST_MUX,ACTIVO_BAJO)

#define 	MAX_DISPLAY_VALUE	999


/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//								MACROS

/**************************************************************************/

#define CENTENA(X)				(X/100)
#define DECENA(X)				((X%100)/10)
#define UNIDAD(X)				(X%10)

#define DISPLAY_DIGIT_OFF(X)	SetDisplayDigit(ARBITRARY_VALUE,X)

/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//							DATA TYPES

/**************************************************************************/



/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//							GLOBAL VARIABLES

/**************************************************************************/

extern volatile unsigned char BufferDisplay[];

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//						FUNCTIONS	DECLARATIONS

/**************************************************************************/

void Display_Inicializacion (void);

void setDIGIT(char digito);

void Display_Barrrido (void);

void Display(uint16_t Val, uint8_t dsp);

void SetDisplayDigit(uint8_t Val, uint8_t digit);

/**************************************************************************/
/**************************************************************************/

#endif /* FW_DISPLAY_H_ */
