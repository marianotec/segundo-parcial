/** ==============================================
 *
 * @file		FW_Timers.h
 *
 *
 * @brief		Este archivo contiene las estructuras básicas y los defines necesarios para manejar los Timers del LPC1769
 *
 * @author		Mariano Koremblum,	marianotec7@gmail.com
 *
 * @date 		11/11/2013
 *
	============================================== */

#ifndef FW_TIMERS_H_
#define FW_TIMERS_H_

/**************************************************************************/

//								INCLUDES

/**************************************************************************/

#include "FW_RegsLPC1769.h"

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//								DEFINES

/**************************************************************************/

#define TIMER0						((timers_t *)		(0x40004000))
#define TIMER1						((timers_t *)		(0x40008000))
#define TIMER2						((timers_t *)		(0x40090000))
#define TIMER3						((timers_t *)		(0x40094000))

#define MAX_TIMERS 					10

///VALOR ARBITRARIO QUE NO CORRESPONDE A UN VALOR DE TIMER VALIDO
#define BAD_TIMER_VALUE				99

#define BAD_TIMER_INIT_ARGUMETS		((clksrc > 3) || (Tmr > 3))

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#define INIT_VALUE_MR0				50//5000 //todo
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ¿QUE VALOR VA???

#define COUNTER_CAPTURE_EDGE		1	///RISING EDGE

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//								MACROS

/**************************************************************************/



/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//								DATA TYPES

/**************************************************************************/

/*!
 * The Timer 0, 1, 2, and 3 peripherals are configured using the following registers:

 *	1. Power: In the PCONP register (Table 46), set bits PCTIM0/1/2/3. Remark: On reset, Timer0/1 are enabled (PCTIM0/1 = 1), and Timer2/3 are disabled (PCTIM2/3 = 0).

 *	2. Peripheral clock: In the PCLKSEL0 register (Table 40), select PCLK_TIMER0/1; in the PCLKSEL1 register (Table 41), select PCLK_TIMER2/3.

 *	3. Pins: Select timer pins through the PINSEL registers. Select the pin modes for the port pins with timer functions through the PINMODE registers (Section 8.5).

 *	4. Interrupts: See register T0/1/2/3MCR (Table 429) and T0/1/2/3CCR (Table 430) for match and capture events. Interrupts are enabled in the NVIC using the appropriate Interrupt Set Enable register.

 *	5. DMA: Up to two match conditions can be used to generate timed DMA requests, see Table 543.
 **/
typedef struct
{
	union
	{
		/**  "TnIR": Interrupt Register.
		 *
		 * The IR can be written to clear interrupts. The IR can be read to identify which of eight possible interrupt sources are pending.
		 *
		 * If an interrupt is generated then the corresponding bit in the IR will be high.
		 * Otherwise, the bit will be low. Writing a logic one to the corresponding IR bit will reset the interrupt.
		 * Writing a zero has no effect.
		 * The act of clearing an interrupt for a timer match also clears any corresponding DMA request.
		 * */
		__RW uint32_t	_IR;

		struct
		{
			__RW uint32_t	_MR0_INTERRUPT:1;
			__RW uint32_t	_MR1_INTERRUPT:1;
			__RW uint32_t	_MR2_INTERRUPT:1;
			__RW uint32_t	_MR3_INTERRUPT:1;
			__RW uint32_t	_CR0_INTERRUPT:1;
			__RW uint32_t	_CR1_INTERRUPT:1;

			__R uint32_t	_IR_RESERVED:26;
		};
	};


	union
	{
		/**  Timer Control Register. The TCR is used to control the Timer
		 *	Counter functions. The Timer Counter can be disabled or reset
		 *	through the TCR.*/
		__RW uint32_t	_TCR;

		struct
		{
			__RW uint32_t	_COUNTER_ENABLE:1;
			__RW uint32_t	_COUNTER_RESET:1;

			__R uint32_t	_TCR_RESERVED:30;
		};
	};

	/** The 32-bit Timer Counter register is incremented when the prescale counter reaches its
	 * terminal count. Unless it is reset before reaching its upper limit, the Timer Counter will
	 * count up through the value 0xFFFF FFFF and then wrap back to the value 0x0000 0000.
	 * This event does not cause an interrupt, but a match register can be used to detect an
	 * overflow if needed.*/
	__RW uint32_t	_TC;


	/** The 32-bit Prescale register specifies the maximum value for the Prescale Counter.*/
	__RW uint32_t	_PR;



	/**The 32-bit Prescale Counter controls division of PCLK by some constant value before it is
	 * applied to the Timer Counter. This allows control of the relationship of the resolution of the
	 * timer versus the maximum time before the timer overflows. The Prescale Counter is
	 * incremented on every PCLK. When it reaches the value stored in the Prescale register,
	 * the Timer Counter is incremented and the Prescale Counter is reset on the next PCLK.
	 * This causes the Timer Counter to increment on every PCLK when PR = 0, every 2 pclks
	 * when PR = 1, etc. */
	__RW uint32_t	_PC;

	union
	{
		__RW uint32_t	_MCR;

		struct
		{
			__RW uint32_t	_MR0I:1;
			__RW uint32_t	_MR0R:1;
			__RW uint32_t	_MR0S:1;

			__RW uint32_t	_MR1I:1;
			__RW uint32_t	_MR1R:1;
			__RW uint32_t	_MR1S:1;

			__RW uint32_t	_MR2I:1;
			__RW uint32_t	_MR2R:1;
			__RW uint32_t	_MR2S:1;

			__RW uint32_t	_MR3I:1;
			__RW uint32_t	_MR3R:1;
			__RW uint32_t	_MR3S:1;

			__R uint32_t	_MCR_RESERVED:20;

		};
	};

	///@{
	/** The Match register values are continuously compared to the Timer Counter value. When
	 * the two values are equal, actions can be triggered automatically. The action possibilities
	 * are to generate an interrupt, reset the Timer Counter, or stop the timer. Actions are
	 * controlled by the settings in the MCR register.
	 */

	__RW uint32_t	_MR0;
	__RW uint32_t	_MR1;
	__RW uint32_t	_MR2;
	__RW uint32_t	_MR3;
	///@}

	union
	{
		/**The Capture Control Register is used to control whether one of the four Capture Registers
		 * is loaded with the value in the Timer Counter when the capture event occurs, and whether
		 * an interrupt is generated by the capture event. Setting both the rising and falling bits at the
		 * same time is a valid configuration, resulting in a capture event for both edges. In the
		 * description below, "n" represents the Timer number, 0 or 1.
		 * Note: If Counter mode is selected for a particular CAP input in the CTCR, the 3 bits for
		 * that input in this register should be programmed as 000, but capture and/or interrupt can
		 * be selected for the other 3 CAP inputs.*/
		__RW uint32_t	_CCR;

		struct
		{
			__RW uint32_t	_CAP0RE:1;
			__RW uint32_t	_CAP0FE:1;
			__RW uint32_t	_CAP0I:1;

			__RW uint32_t	_CAP1RE:1;
			__RW uint32_t	_CAP1FE:1;
			__RW uint32_t	_CAP1I:1;

			__R uint32_t	_CCR_RESERVED:26;
		};
	};

	/** Each Capture register is associated with a device pin and may be loaded with the Timer
	 * Counter value when a specified event occurs on that pin. The settings in the Capture
	 * Control Register register determine whether the capture function is enabled, and whether
	 * a capture event happens on the rising edge of the associated pin, the falling edge, or on
	 * both edges.*/
	__R uint32_t	_CR0;

	/** Each Capture register is associated with a device pin and may be loaded with the Timer
	 * Counter value when a specified event occurs on that pin. The settings in the Capture
	 * Control Register register determine whether the capture function is enabled, and whether
	 * a capture event happens on the rising edge of the associated pin, the falling edge, or on
	 * both edges.*/
	__R uint32_t	_CR1;


	__R uint32_t	_RESERVED0;
	__R uint32_t	_RESERVED1;

	union
	{
		/**The External Match Register provides both control and status of the external match pins.
		 * In the descriptions below, “n” represents the Timer number, 0 or 1, and “m” represent a
		 * Match number, 0 through 3.*/
		__RW uint32_t	_EMR;

		struct
		{
			__RW uint32_t	_EM0:1;
			__RW uint32_t	_EM1:1;
			__RW uint32_t	_EM2:1;
			__RW uint32_t	_EM3:1;

			__RW uint32_t	_EMC0:1;
			__RW uint32_t	_EMC1:1;
			__RW uint32_t	_EMC2:1;
			__RW uint32_t	_EMC3:1;

			__R uint32_t		_EMR_RESERVED:24;
		};
	};

	__R uint32_t	_RESERVED2;
	__R uint32_t	_RESERVED3;
	__R uint32_t	_RESERVED4;
	__R uint32_t	_RESERVED5;
	__R uint32_t	_RESERVED6;
	__R uint32_t	_RESERVED7;
	__R uint32_t	_RESERVED8;
	__R uint32_t	_RESERVED9;
	__R uint32_t	_RESERVED10;
	__R uint32_t	_RESERVED11;
	__R uint32_t	_RESERVED12;
	__R uint32_t	_RESERVED13;

	union
	{
		/** Count Control Register. The CTCR selects between Timer and
		 * Counter mode, and in Counter mode selects the signal and edge(s)
		 * for counting
		 */
		__RW uint32_t	_CTCR;

		struct
		{
			__RW uint32_t	_C_T_MODE:2;

			__RW uint32_t	_COUNT_IN_SELECT:2;

			__RW uint32_t	_CTCT_RESERVED:28;
		};
	};

} timers_t;

/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//						   GLOBAL VARIABLES

/**************************************************************************/

extern volatile uint32_t BufferTimers[ MAX_TIMERS ];

extern volatile uint8_t BufferEventos[ MAX_TIMERS ];

extern volatile bool fTimer0 ;

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//						  FUNCTIONS	DECLARATIONS

/**************************************************************************/

void TIMER0_Inicializacion( void );

void TimerStatus( void );
void TimerClose ( void );
void TimerStop ( uint8_t ev );
void TimerStart ( uint8_t ev , uint8_t t );

/**************************************************************************/
/**************************************************************************/

#endif /* FW_TIMERS_H_ */
