/** ==============================================
 *
 * @file		FW_PINSEL.h
 *
 *
 * @brief
 *
 * @author		Mariano Koremblum,	marianotec7@gmail.com
 *
 * @date 		11/11/2013
 *
 	============================================== */

#ifndef FW_PINSEL_H_
#define FW_PINSEL_H_

//** ************************************************************** **/
//  INCLUDES
//** ************************************************************** **/

#include "FW_RegsLPC1769.h"

//** ************************************************************** **/


//** ************************************************************** **/
//  						DEFINES
//** ************************************************************** **/

/** 					Registros PINSEL
 	 	 00	GPIO (reset value)		01	Función 1
		 11	Función 3				10	Función 2
**/
#define 	PINSEL					((pinsel_t *)	0x4002C000)

#define		ERROR_BAD_ARGUMENT		while(1){printf("ERROR!!!\n\n");}

//** ************************************************************** **/

//** ************************************************************** **/
//  						PROTOTYPES
//** ************************************************************** **/

/**
 *
 * @param port
 * @param pin
 * @param function
 */
void SetPINSEL( uint8_t port , uint8_t pin ,uint8_t function);

//** ************************************************************** **/

//** ************************************************************** **/
//  TYPEDEF's
//** ************************************************************** **/

typedef struct
{
	union
	{
		/** The LOW_PINSEL register controls the functions of the lower half of Port's.
		 * The direction control bit in FIOnDIR register is effective only when the
		 * GPIO function is selected for a pin. For other functions,
		 * the direction is controlled automatically.
		 * */
		__RW uint32_t	_LOW_PINSEL;


		struct
		{
			__RW uint32_t	_Pn_0:2;

			__RW uint32_t	_Pn_1:2;

			__RW uint32_t	_Pn_2:2;

			__RW uint32_t	_Pn_3:2;

			__RW uint32_t	_Pn_4:2;

			__RW uint32_t	_Pn_5:2;

			__RW uint32_t	_Pn_6:2;

			__RW uint32_t	_Pn_7:2;

			__RW uint32_t	_Pn_8:2;

			__RW uint32_t	_Pn_9:2;

			__RW uint32_t	_Pn_10:2;

			__RW uint32_t	_Pn_11:2;

			__RW uint32_t	_Pn_12:2;

			__RW uint32_t	_Pn_13:2;

			__RW uint32_t	_Pn_14:2;

			__RW uint32_t	_Pn_15:2;
		};
	};

	union
	{
		/**
		 * The HIGH_PINSEL register controls the functions of the upper half of Port's.
		 *  The direction control bit in the FIOnDIR register is effective only when the
		 *  GPIO function is selected for a pin.
		 *  For other functions the direction is controlled automatically.
		 */
		__RW uint32_t	_HIGH_PINSEL;

		struct
		{
			__RW uint32_t	_Pn_16:2;

			__RW uint32_t	_Pn_17:2;

			__RW uint32_t	_Pn_18:2;

			__RW uint32_t	_Pn_19:2;

			__RW uint32_t	_Pn_20:2;

			__RW uint32_t	_Pn_21:2;

			__RW uint32_t	_Pn_22:2;

			__RW uint32_t	_Pn_23:2;

			__RW uint32_t	_Pn_24:2;

			__RW uint32_t	_Pn_25:2;

			__RW uint32_t	_Pn_26:2;

			__RW uint32_t	_Pn_27:2;

			__RW uint32_t	_Pn_28:2;

			__RW uint32_t	_Pn_29:2;

			__RW uint32_t	_Pn_30:2;

			__RW uint32_t	_Pn_31:2;
		};
	};
} pinsel_t;

//** ************************************************************** **/


#endif /* FW_PINSEL_H_ */
