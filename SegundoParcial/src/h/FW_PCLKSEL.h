/*
 * PCLKSEL.h
 *
 *  Created on: 20/09/2013
 *      Author: Mariano Koremblum
 */

#ifndef PCLKSEL_H_
#define PCLKSEL_H_

#include "FW_RegsLPC1769.h"

//!< ///////////////////   PCLKSEL   //////////////////////////
//!< Peripheral Clock Selection registers 0 and 1 (PCLKSEL0 -0x400F C1A8 and PCLKSEL1 - 0x400F C1AC) [pag. 56 user manual]
//!< 0x400FC1A8UL : Direccion de inicio de los registros de seleccion de los CLKs de los dispositivos:
#define		PCLKSEL		((pclksel_t*) 0x400FC1A8UL)

typedef struct
{
	__RW uint32_t PCLK_WDT				:2;
	__RW uint32_t PCLK_TIMER0			:2;
	__RW uint32_t PCLK_TIMER1			:2;
	__RW uint32_t PCLK_UART0			:2;
	__RW uint32_t PCLK_UART1			:2;
	__RW uint32_t RESERVED1				:2;
	__RW uint32_t PCLK_PWM1				:2;
	__RW uint32_t PCLK_I2C0				:2;
	__RW uint32_t PCLK_SPI				:2;
	__RW uint32_t RESERVED2				:2;
	__RW uint32_t PCLK_SSP1				:2;//bit 20
	__RW uint32_t PCLK_DAC				:2;
	__RW uint32_t PCLK_ADC				:2;
	__RW uint32_t PCLK_CAN1				:2;
	__RW uint32_t PCLK_CAN2				:2;
	__RW uint32_t PCLK_ACF				:2;
	__RW uint32_t PCLK_QEI				:2;//2nd BYTE
	__RW uint32_t PCLK_GPIOINT			:2;
	__RW uint32_t PCLK_PCB				:2;
	__RW uint32_t PCLK_I2C1				:2;
	__RW uint32_t RESERVED3				:2;//bit 40
	__RW uint32_t PCLK_SSP0				:2;
	__RW uint32_t PCLK_TIMER2			:2;
	__RW uint32_t PCLK_TIMER3			:2;
	__RW uint32_t PCLK_UART2			:2;
	__RW uint32_t PCLK_UART3			:2;
	__RW uint32_t PCLK_I2C2				:2;
	__RW uint32_t PCLK_I2S				:2;
	__RW uint32_t RESERVED4				:2;
	__RW uint32_t PCLK_RIT				:2;
	__RW uint32_t PCLK_SYSCON			:2;//bit 60
	__RW uint32_t PCLK_MC				:2;

} pclksel_t;

#endif /* PCLKSEL_H_ */
