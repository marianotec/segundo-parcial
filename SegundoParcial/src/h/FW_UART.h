/** ==============================================
 *
 * @file		FW_UART.h
 *
 *
 * @brief		Este archivo contiene las estructuras básicas y los defines necesarios para manejar las UART del LPC1769
 *
 * @author		Mariano Koremblum,	marianotec7@gmail.com
 *
 * @date 		11/11/2013
 *
	============================================== */

#ifndef FW_UART_H_
#define FW_UART_H_

/**************************************************************************/

//										INCLUDES

/**************************************************************************/

#include "FW_RegsLPC1769.h"

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//										DEFINES

/**************************************************************************/

#define 	TXD0					0,2
#define 	TXD1					0,15
#define 	TXD2					0,10
#define 	TXD3					0,0

#define 	RXD0					0,3
#define 	RXD1					0,16
#define 	RXD2					0,11
#define 	RXD3					0,1

#define		MAX_RX0					16
#define		MAX_TX0 				16

#define 	MAX_TX					50

#define 	MAX_MESSAGE				16

#define 	WAIT_STRING 			0
#define 	GET_STRING		    	1

#define 	TRAMA_START				(character == '#')
#define		TRAMA_END				(character == '$')
#define		NO_CHARACTER			(character < 0)

#define 	MESSAGE_AVAILABLE		(Message[0]!='\0')
#define 	NOT_AVAILABLE_MESSAGE 	(Message[0]=='\0')

#define THRE_INTERRUPT				1
#define RDA_INTERRUPT				2

//MODIFICAR ESTOS VALORES!!!!!
#define	MAX_MESSAGE_SIZE			(15)

///Puntero a la estructura correspondiente a la UART 0
#define UART0						((uart_t *)		0x4000C000)
///Puntero a la estructura correspondiente a la UART 1
#define UART1						((uart1_t *)	0x40010000)
///Puntero a la estructura correspondiente a la UART 2
#define UART2						((uart_t *)		0x40098000)
///Puntero a la estructura correspondiente a la UART 3
#define UART3						((uart_t *) 	0x4009C000)

#define INVALID_CHARACTER			99

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//										MACROS

/**************************************************************************/

#define START_TX(X)					UART0->_THR = X

/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//										DATA TYPES

/**************************************************************************/


/// Estructura UART 1
typedef struct
{
	///Receiver Buffer & Transmit Holding & Divisor Latch LSB Registers
	union
	{
		/**The U1RBR is the top byte of the UART1 RX FIFO. The top byte of the RX FIFO contains
		 * the oldest character received and can be read via the bus interface. The LSB (bit 0)
		 * represents the “oldest” received data bit. If the character received is less than 8 bits, the
		 * unused MSBs are padded with zeroes.
		 * The Divisor Latch Access Bit (DLAB) in U1LCR must be zero in order to access the
		 * U1RBR. The U1RBR is always read-only.
		 * Since PE, FE and BI bits correspond to the byte sitting on the top of the RBR FIFO (i.e.
		 * the one that will be read in the next read from the RBR), the right approach for fetching the
		 * valid pair of received byte and its status bits is first to read the content of the U1LSR
		 * register, and then to read a byte from the U1RBR.
		 *
		 * IF DLAB = 0 && READ!!!!
		 */
		__R uint32_t	U1RBR;
		struct
		{
			///The UART1 Receiver Buffer Register contains the oldest received byte in the UART1 RX FIFO.
			__R uint32_t	_RBR:8;//Receiver Buffer Register

			__R uint32_t	_U1RBR_RESERVED:24;
		};

		/**
		 * The write-only U1THR is the top byte of the UART1 TX FIFO. The top byte is the newest
		 * character in the TX FIFO and can be written via the bus interface. The LSB represents the
		 * first bit to transmit.
		 * The Divisor Latch Access Bit (DLAB) in U1LCR must be zero in order to access the
		 * U1THR. The U1THR is write-only.
		 *
		 * IF DLAB = 0 && WRITE!!!!
		 * */
		__W uint32_t	U1THR;
		struct
		{
			/**
			 * Writing to the UART1 Transmit Holding Register causes the data to be stored in the UART1
			 * transmit FIFO. The byte will be sent when it reaches the bottom of the FIFO and the
			 * transmitter is available.
			 * */
			__W uint32_t	_THR:8;//Transmitter Holding Register

			__R uint32_t	_U1THR_RESERVED:24;
		};

		/**
		 * The UART1 Divisor Latch is part of the UART1 Baud Rate Generator and holds the value
		 * used, along with the Fractional Divider, to divide the APB clock (PCLK) in order to produce
		 * the baud rate clock, which must be 16x the desired baud rate. The U1DLL and U1DLM
		 * registers together form a 16-bit divisor where U1DLL contains the lower 8 bits of the
		 * divisor and U1DLM contains the higher 8 bits of the divisor. A 0x0000 value is treated like
		 * a 0x0001 value as division by zero is not allowed.The Divisor Latch Access Bit (DLAB) in
		 * U1LCR must be one in order to access the UART1 Divisor Latches.
		 *
		 * IF DLAB = 1!!!!
		 * */
		__RW uint32_t	U1DLL;
		struct
		{
			__RW uint32_t	_DLLSB:8;

			__R uint32_t	_U1DLL_RESERVED:24;
		};
	};


	///Divisor Latch MSB & Interrupt Enable Registers
	union
	{
		/**
		 * The UART1 Divisor Latch MSB Register, along with the U1DLL register, determines the
		 * baud rate of the UART1.
		 * DLAB = 1
		 */
		__RW uint32_t U1DLM;
		struct
		{
			__RW uint32_t	_DLMSB:8;

			__R uint32_t	_U1DLM_RESERVED:24;
		};

		///The U1IER is used to enable the four UART1 interrupt sources.
		__RW uint32_t U1IER;
		struct
		{
			__RW uint32_t	_RBR_Interrupt_Enable:1;

			__RW uint32_t	_THRE_Interrupt_Enable:1;

			__RW uint32_t	_RX_Line_Interrupt_Enable:1;

			__RW uint32_t	_Modem_Status_Interrupt_Enable:1;

			__R uint32_t	_U1IER_RESERVED0:3;

			/**
			 * If auto-cts mode is enabled this bit enables/disables the modem status interrupt
			 * generation on a CTS1 signal transition. If auto-cts mode is disabled a CTS1 transition
			 * will generate an interrupt if Modem Status Interrupt Enable (U1IER[3]) is set.
			 * In normal operation a CTS1 signal transition will generate a Modem Status Interrupt
			 * unless the interrupt has been disabled by clearing the U1IER[3] bit in the U1IER
			 * register. In auto-cts mode a transition on the CTS1 bit will trigger an interrupt only if both
			 * the U1IER[3] and U1IER[7] bits are set.
			 * */
			__RW uint32_t	_CTS_Interrupt_Enable:1;

			///Enables the end of auto-baud interrupt.
			__RW uint32_t	_ABEO_Interrupt_Enable:1;

			///Enables the auto-baud time-out interrupt.
			__RW uint32_t	_ABTO_Interrupt_Enable:1;

			__R uint32_t	_U1IER_RESERVED1:22;
		};
	};

	///Interrupt ID Register. Identifies which interrupt(s) are pending.
	union
	{
		/**
		 * Interrupt ID Register. Identifies which interrupt(s) are pending.
		 *
		 * The U1IIR provides a status code that denotes the priority and source of a pending
		 * interrupt. The interrupts are frozen during an U1IIR access. If an interrupt occurs during
		 * an U1IIR access, the interrupt is recorded for the next U1IIR access.
		 * */
		__R uint32_t U1IIR;

		struct
		{
			///¡¡ACTIVO BAJO!!
			__R uint32_t	_Interrupt_Status:1;

			__R uint32_t	_Interrupt_ID:3;

			__R uint32_t	_U1IIR_RESERVED0:2;

			__R uint32_t	_FIFO_Enable_read:2;

			/**End of auto-baud interrupt. True if auto-baud has finished successfully andinterrupt is enabled.*/
			__R uint32_t	_ABEOint:1;

			/**Auto-baud time-out interrupt. True if auto-baud has timed out and interrupt is enabled.*/
			__R uint32_t	_ABTOint:1;

			__R uint32_t	_U1IIR_RESERVED1:22;
		};


		/// FIFO Control Register. Controls UART1 FIFO usage and modes.
		__W uint32_t U1FCR;
		struct
		{
			__W uint32_t	_FIFO_Enable:1;

			__W uint32_t	_RX_FIFO_RESET:1;

			__W uint32_t	_TX_FIFO_RESET:1;

			__W uint32_t	_DMA_Mode_Select:1;

			///End of auto-baud interrupt. True if auto-baud has finished successfully andinterrupt is enabled.
			__W uint32_t	_U1FCR_RESERVED0:2;

			///Auto-baud time-out interrupt. True if auto-baud has timed out and interrupt is enabled.
			__W uint32_t	_RX_Trigger_Level:2;

			__W uint32_t	_U1FCR_RESERVED1:24;
		};
	};

	///Line Control Register. Contains controls for frame formatting and break generation.
	union
	{
		///Line Control Register. Contains controls for frame formatting and break generation.
		__RW uint32_t U1LCR;

		struct
		{
			/**
			 * 00 5-bit character length.
			 *
			 * 01 6-bit character length.
			 *
			 * 10 7-bit character length.
			 *
			 * 11 8-bit character length.
			 * */
			__RW uint32_t _Word_Lenght_Select:2;

			__RW uint32_t _Stop_Bit_Select:1;

			__RW uint32_t _Parity_Enable:1;

			/**
			 * 00 Odd parity. Number of 1s in the transmitted character and the attached parity bit will be odd.
			 *
			 * 01 Even Parity. Number of 1s in the transmitted character and the attached parity bit will be even.
			 *
			 * 10 Forced "1" stick parity.
			 *
			 * 11 Forced "0" stick parity.
			 * */
			__RW uint32_t _Parity_Select:2;

			__RW uint32_t _Break_Control:1;

			__RW uint32_t _DLAB:1;
		};

	};


	/**Modem Control Register. Contains controls for flow control
	handshaking and loopback mode.*/
	__RW uint32_t U1MCR;

	///Line Status Register. Contains flags for transmit and receive status, including line errors.
	union
	{
		///Line Status Register. Contains flags for transmit and receive status, including line errors.
		__R uint32_t U1LSR;

		struct
		{
			///Is set when the U1RBR holds an unread character and is cleared when the UART1 RBR FIFO is empty.
			__R uint32_t _Receiver_Data_Ready:1;

			__R uint32_t _Overrun_Error:1;

			__R uint32_t _Parity_Error:1;

			__R uint32_t _Framing_Error:1;

			__R uint32_t _Break_Interrupt:1;

			__R uint32_t _Transmitter_Holding_Register_Empty:1;

			__R uint32_t _Transmitter_Empty:1;

			__R uint32_t _RX_FIFO_ERROR:1;

			__R uint32_t _U1LSR_RESERVED:24;
		};

	};

	/**Modem Status Register. Contains handshake signal status flags.*/
	__R uint32_t U1MSR;

	/**Scratch Pad Register. 8-bit temporary storage for software.*/
	__RW uint32_t U1SCR;

	/**Auto-baud Control Register. Contains controls for the auto-baud feature.*/
	__RW uint32_t U1ACR;

	__R uint32_t RESERVED0;

	/**Fractional Divider Register. Generates a clock input for the baud rate divider.*/
	__RW uint32_t U1FDR;

	__R uint32_t RESERVED1;

	/**Transmit Enable Register. Turns off UART transmitter for use with	software flow control.*/
	__RW uint32_t U1TER;

	__R uint32_t RESERVED2;
	__R uint32_t RESERVED3;
	__R uint32_t RESERVED4;
	__R uint32_t RESERVED5;
	__R uint32_t RESERVED6;
	__R uint32_t RESERVED7;

	/** RS-485/EIA-485 Control. Contains controls to configure various aspects of RS-485/EIA-485 modes.*/
	__RW uint32_t U1RS485CTRL;

	/** RS-485/EIA-485 address match. Contains the address match value for RS-485/EIA-485 mode.*/
	__RW uint32_t U1ADRMATCH;

	/** RS-485/EIA-485 direction control delay.*/
	__RW uint32_t U1RS485DLY;

} uart1_t;

/*****************************************************************************************************/

/// Estructura UART 0, 2, 3
typedef struct
{
	///Receiver Buffer & Transmit Holding & Divisor Latch LSB Registers
	union
	{
		/**
		 * The UnRBR is the top byte of the UARTn Rx FIFO. The top byte of the Rx FIFO contains
		 * the oldest character received and can be read via the bus interface. The LSB (bit 0)
		 * represents the “oldest” received data bit. If the character received is less than 8 bits, the
		 * unused MSBs are padded with zeroes.
		 * The Divisor Latch Access Bit (DLAB) in LCR must be zero in order to access the UnRBR.
		 * The UnRBR is always read-only.
		 * Since PE, FE and BI bits correspond to the byte sitting on the top of the RBR FIFO (i.e.
		 * the one that will be read in the next read from the RBR), the right approach for fetching the
		 * valid pair of received byte and its status bits is first to read the content of the U0LSR
		 * register, and then to read a byte from the UnRBR.
		 *
		 * IF DLAB = 0 && READ!!!!
		 * */
		struct
		{
			/**The UARTn Receiver Buffer Register contains the oldest received byte in the UARTn RX FIFO.*/
			__R uint32_t	_RBR:8;//Receiver Buffer Register

			__R uint32_t	_RBR_RESERVED:24;
		};

		/**
		 * The UnTHR is the top byte of the UARTn TX FIFO. The top byte is the newest character in
		 * the TX FIFO and can be written via the bus interface. The LSB represents the first bit to
		 * transmit.
		 * The Divisor Latch Access Bit (DLAB) in UnLCR must be zero in order to access the
		 * UnTHR. The UnTHR is always write-only.
		 *
		 * IF DLAB = 0 && WRITE!!!!
		 * */
		struct
		{
			/**
			 * Writing to the UARTn Transmit Holding Register causes the data to be stored in the UARTn
			 * transmit FIFO. The byte will be sent when it reaches the bottom of the FIFO and the
			 * transmitter is available.
			 * */
			__W uint32_t	_THR:8;//Transmitter Holding Register

			__R uint32_t	_THR_RESERVED:24;
		};

		/**
		 * The UARTn Divisor Latch is part of the UARTn Baud Rate Generator and holds the value
		 * used, along with the Fractional Divider, to divide the APB clock (PCLK) in order to produce
		 * the baud rate clock, which must be 16× the desired baud rate. The UnDLL and UnDLM
		 * registers together form a 16-bit divisor where UnDLL contains the lower 8 bits of the
		 * divisor and UnDLM contains the higher 8 bits of the divisor. A 0x0000 value is treated like
		 * a 0x0001 value as division by zero is not allowed. The Divisor Latch Access Bit (DLAB) in
		 * UnLCR must be one in order to access the UARTn Divisor Latches.
		 *
		 * If DLAB = 1!!!!
		 * */
		struct
		{
			/**
			 * The UARTn Divisor Latch LSB Register, along with the UnDLM register,
			 * determines the baud rate of the UARTn.
			 * */
			__RW uint32_t	_DLLSB:8;//DLAB = 1

			__R uint32_t	_DLL_RESERVED:24;
		};
	};

	///Divisor Latch MSB & Interrupt Enable Register Union
	union
	{
		///If DLAB = 1!!!!
		struct
		{
			/**
			 * The UARTn Divisor Latch MSB Register, along with the U0DLL register,
			 * determines the baud rate of the UARTn.
			 * */
			__RW uint32_t	_DLMSB:8;

			__R uint32_t	_DLM_RESERVED:24;
		};

		/**The IER is used to enable the four UARTn interrupt sources.
		 *
		 * If DLAB = 0!!!!
		 * */
		__RW uint32_t IER;//DLAB = 0
		struct
		{
			///Habilita la interrupción por RX
			__RW uint32_t	_RBR_Interrupt_Enable:1;

			///Habilita la interrupción por TX (Buffer de salida vacío)
			__RW uint32_t	_THRE_Interrupt_Enable:1;

			__RW uint32_t	_RX_Line_Status_Interrupt_Enable:1;

			__R uint32_t	_IER_RESERVED0:5;

			/**Enables the end of auto-baud interrupt.*/
			__RW uint32_t	_ABEO_Interrupt_Enable:1;

			/**Enables the auto-baud time-out interrupt.*/
			__RW uint32_t	_ABTO_Interrupt_Enable:1;

			__R uint32_t	_IER_RESERVED1:22;
		};
	};


	///Interrupt ID Register Union
	union
	{
		/**
		 * Interrupt ID Register. Identifies which interrupt(s) are pending.
		 * The IIR provides a status code that denotes the priority and source of a pending
		 * interrupt. The interrupts are frozen during an IIR access. If an interrupt occurs during
		 * an IIR access, the interrupt is recorded for the next IIR access.
		 * */
		__R uint32_t IIR;

		struct
		{
			///Si esta en 0 significa que hay una interrupción pendiente, caso contrario si se encuentra en 1 (Es activo bajo)
			__R uint32_t	_Interrupt_Status:1;

			__R uint32_t	_Interrupt_ID:3;

			__R uint32_t	_IIR_RESERVED0:2;

			__R uint32_t	_FIFO_Enable_read:2;

			/**End of auto-baud interrupt. True if auto-baud has finished successfully andinterrupt is enabled.*/
			__R uint32_t	_ABEOint:1;

			/**Auto-baud time-out interrupt. True if auto-baud has timed out and interrupt is enabled.*/
			__R uint32_t	_ABTOint:1;

			__R uint32_t	_IIR_RESERVED1:22;
		};


		/** FIFO Control Register. Controls UARTn FIFO usage and modes.*/
		__W uint32_t FCR;
		struct
		{
			__W uint32_t	_FIFO_Enable:1;

			__W uint32_t	_RX_FIFO_Reset:1;

			__W uint32_t	_TX_FIFO_Reset:1;

			__W uint32_t	_DMA_Mode_Select:1;

			__W uint32_t	_FCR_RESERVED0:2;

			/**Auto-baud time-out interrupt. True if auto-baud has timed out and interrupt is enabled.*/
			__W uint32_t	_RX_Trigger_Level:2;

			__W uint32_t	_FCR_RESERVED1:24;
		};
	};

	union
	{
		/** Line Control Register. Contains controls for frame formatting and break generation.*/
		__RW uint32_t LCR;

		struct
		{
			/**
			 * 00 5-bit character length.
			 *
			 * 01 6-bit character length.
			 *
			 * 10 7-bit character length.
			 *
			 * 11 8-bit character length.
			 * */
			__RW uint32_t _Word_Lenght_Select:2;

			__RW uint32_t _Stop_Bit_Select:1;

			__RW uint32_t _Parity_Enable:1;

			/**
			 * 00 Odd parity. Number of 1s in the transmitted character and the attached parity bit will be odd.
			 *
			 * 01 Even Parity. Number of 1s in the transmitted character and the attached parity bit will be even.
			 *
			 * 10 Forced "1" stick parity.
			 *
			 * 11 Forced "0" stick parity.
			 * */
			__RW uint32_t _Parity_Select:2;

			__RW uint32_t _Break_Control:1;

			__RW uint32_t _DLAB:1;

			__R uint32_t  _LCRRESERVED:24;
		};

	};

	__R uint32_t RESERVED0;

	union
	{
		/**
		 * Line Status Register. Contains flags for transmit and receive status, including line errors.
		 * */
		__R uint32_t LSR;

		struct
		{
			/**Is set when the RBR holds an unread character and is cleared when the UARTn RBR FIFO is empty.*/
			__R uint32_t _Receiver_Data_Ready:1;

			__R uint32_t _Overrun_Error:1;

			__R uint32_t _Parity_Error:1;

			__R uint32_t _Framing_Error:1;

			__R uint32_t _Break_Interrupt:1;

			__R uint32_t _Transmitter_Holding_Register_Empty:1;

			__R uint32_t _Transmitter_Empty:1;

			__R uint32_t _RX_FIFO_ERROR:1;

			__R uint32_t LSR_RESERVED:24;
		};

	};

	__R uint32_t RESERVED1;

	/**Scratch Pad Register. 8-bit temporary storage for software.*/
	__RW uint32_t SCR;

	/**Auto-baud Control Register. Contains controls for the auto-baud feature.*/
	__RW uint32_t ACR;

	__RW uint32_t ICR;

	/**Fractional Divider Register. Generates a clock input for the baud rate divider.*/
	__RW uint32_t FDR;

	/**Transmit Enable Register. Turns off UART transmitter for use with	software flow control.*/
	__RW uint32_t TER;
} uart_t;

/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//							 GLOBAL VARIABLES

/**************************************************************************/

extern volatile bool 		TX_inx_in; // Buffer Índice de entrada del buffer de Tx

extern volatile bool 		TX_inx_out; // Buffer Índice de salida del buffer de Tx

extern volatile bool 		txStart;

extern volatile char 		BufferTx[];

extern volatile char		Message[];

extern volatile char		uart0_buffer[];
extern volatile bool 		uart0_buffer_available;

extern volatile char  		BuffRX0[MAX_RX0];
extern volatile char	  	BuffTX0[MAX_TX0];

extern volatile uint8_t		IndiceBuff_InRX0;
extern volatile uint8_t		IndiceBuff_OutRX0;

extern volatile uint8_t 	tx_buffer_full;
extern volatile uint8_t 	tx_buffer_empty;

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//						  FUNCTIONS	DECLARATIONS

/**************************************************************************/

void UART0_Inicializacion(void);


/*!
 * \fn 		void SendUART(void);
 * \brief 	Esta función envía por la UART el contenido del buffer de dicha UART
 * \return 	void
 */
void SendUART(void);

uint8_t PopTx( uint8_t *dato );

uint8_t PushTx(uint8_t dato);

/**
 *
 * @param uart		Indica en que UART se desea escribir
 * @param string	String que se desea enviar por UART
 */
void WriteUart(uint8_t uart, char * string);

///Función principal de la Máquina de estados que toma la string de recepción si es que la misma llego correctamente
void GetMessage (void);
/**
 *
 * @param dato Caracter que se desea meter en la fila
 * @param uart Indica a que UART corresponde la fila
 */
void PushRX (uint8_t dato);

/**************************************************************************/
/**************************************************************************/

#endif /* FW_UART_H_ */
