/** ==============================================
 *
 * @file		FW_Teclado_Exp3.h
 *
 *
 * @brief	
 *
 * @author		Mariano Koremblum,	marianotec7@gmail.com
 *
 * @date 		18/11/2013
 *
 * @version
 *
	============================================== */

#ifndef FW_TECLADO_EXP3_H_
#define FW_TECLADO_EXP3_H_

/**************************************************************************/

//							INCLUDES

/**************************************************************************/

#include "FW_RegsLPC1769.h"

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//							DEFINES

/**************************************************************************/

#define SW1					8
#define SW2					7
#define SW3					6
#define SW4					5
#define SW5					4
#define SW6					3
#define SW7					2
#define SW8					1

#define MIN_MATCHES			30
#define DELAY_TIMES			50

#define FILA1				EXPANSION7
#define FILA2				EXPANSION8
#define FILA3				EXPANSION9
#define FILA4				EXPANSION10

#define SET_FILA1_ON		SetPIN(FILA1,ACTIVO_ALTO)
#define SET_FILA1_OFF		SetPIN(FILA1,ACTIVO_BAJO)
#define SET_FILA2_ON		SetPIN(FILA2,ACTIVO_ALTO)
#define SET_FILA2_OFF		SetPIN(FILA2,ACTIVO_BAJO)
#define SET_FILA3_ON		SetPIN(FILA3,ACTIVO_ALTO)
#define SET_FILA3_OFF		SetPIN(FILA3,ACTIVO_BAJO)
#define SET_FILA4_ON		SetPIN(FILA4,ACTIVO_ALTO)
#define SET_FILA4_OFF		SetPIN(FILA4,ACTIVO_BAJO)

#define FILA1_ON			GetPIN(FILA1,ACTIVO_ALTO)
#define FILA2_ON			GetPIN(FILA2,ACTIVO_ALTO)
#define FILA3_ON			GetPIN(FILA3,ACTIVO_ALTO)
#define FILA4_ON			GetPIN(FILA4,ACTIVO_ALTO)

#define FILA1_OFF			GetPIN(FILA1,ACTIVO_BAJO)
#define FILA2_OFF			GetPIN(FILA2,ACTIVO_BAJO)
#define FILA3_OFF			GetPIN(FILA3,ACTIVO_BAJO)
#define FILA4_OFF			GetPIN(FILA4,ACTIVO_BAJO)


#define SET_COLUMNA1_ON		SetPIN(EXPANSION11,ACTIVO_ALTO)
#define SET_COLUMNA2_ON		SetPIN(EXPANSION12,ACTIVO_ALTO)
#define SET_COLUMNA1_OFF	SetPIN(EXPANSION11,ACTIVO_BAJO)
#define SET_COLUMNA2_OFF	SetPIN(EXPANSION12,ACTIVO_BAJO)

#define COLUMNA1_ON			GetPIN(EXPANSION11,ACTIVO_ALTO)
#define COLUMNA2_ON			GetPIN(EXPANSION12,ACTIVO_ALTO)

#define GET_KEY				Teclado_Debounce(Teclado_Lectura())

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//							MACROS

/**************************************************************************/



/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//							DATA TYPES

/**************************************************************************/



/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//						  GLOBAL VARIABLES

/**************************************************************************/

extern volatile short Bufferkey;

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//						 FUNCTIONS	DECLARATIONS

/**************************************************************************/

void Teclado_Inicializacion(void);

void Teclado_Debounce(unsigned char ActualKey);

unsigned char Teclado_Lectura(void);

uint8_t Teclado( void );

/**************************************************************************/
/**************************************************************************/

#endif /* FW_TECLADO_EXP3_H_ */
