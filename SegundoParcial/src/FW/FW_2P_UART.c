/** ==============================================
 *
 * @file		FW_2P_UART.c
 *
 *
 * @brief	
 *
 * @author		Mariano Koremblum,	marianotec7@gmail.com
 *
 * @date 		18/11/2013
 *
 * @version
 *
	============================================== */

/**************************************************************************/

//								INCLUDES

/**************************************************************************/

#include "SegundoParcial.h"

/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//						   GLOBAL VARIABLES

/**************************************************************************/

volatile bool 		TX_inx_in; // Buffer Índice de entrada del buffer de Tx
volatile bool 		TX_inx_out; // Buffer Índice de salida del buffer de Tx

volatile bool 		txStart;

volatile char 		BufferTx[MAX_TX];
volatile char  		BuffRX0[MAX_RX0];

volatile char		Message[MAX_MESSAGE];

volatile uint8_t	IndiceBuff_InRX0;
volatile uint8_t	IndiceBuff_OutRX0;

volatile uint8_t 	tx_buffer_full = 0;
volatile uint8_t 	tx_buffer_empty = 1;

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//							FUNCTIONS

/**************************************************************************/

/**
	\fn  void UART0_Inicializacion(void)
	\brief Inicializa a la UART0 en 1200,8,N,1
 	\param void
 	\return void
 */
void UART0_Inicializacion(void)
{
	PCONP->PCUART0 = ON;
	PCLKSEL->PCLK_UART0 = 0;

	UART0->_Word_Lenght_Select = 3;

	UART0->_Stop_Bit_Select = FALSE;

	UART0->_Parity_Enable = FALSE;

	UART0->_Break_Control = FALSE;

	UART0->_DLAB = ON;

	/*
	 * 		     	       PCLK						 25000000
	 * Baudrate = --------------------- => 1200 = -----------------
	 *            16.(256.DLMSB + DLLSB)			  16.DLXX
	 *
	 * DLXX = 1302 = 256.DLMSB + DLLSB
	 */
	UART0->_DLMSB = 5;
	UART0->_DLLSB = 22;

//  .
//	UART0->_FIFO_Enable = ON;
//	UART0->_RX_FIFO_Reset = OFF;

	SetPINSEL(TXD0,FUNCION_1);
	SetPINSEL(RXD0,FUNCION_1);

	UART0->_DLAB = OFF;

	UART0->_RBR_Interrupt_Enable = ON;
	UART0->_THRE_Interrupt_Enable = ON;

	ISER0 |= (1<<5);
}

/**************************************************************************/

/**
	\fn  void UART0_IRQHandler(void)
	\brief Transmite el contenido del buffer de transmisión hasta vaciarlo
 	\param void
 	\return void
 */
void UART0_IRQHandler(void)
{
	uint8_t interruptID, dato;

	do
	{
		//IIR es reset por HW, una vez que lo lei se resetea.
		interruptID = UART0->_Interrupt_ID;
		dato = UART0->IIR;

		if ( interruptID == THRE_INTERRUPT )
		{
			if( !PopTx(&dato) )	UART0->_THR = dato;

			else txStart = 0;
		}

		if ( interruptID == RDA_INTERRUPT ) //Receive Data ready
			PushRX((uint8_t ) UART0->_RBR);

	}while( ! ( UART0->_Interrupt_Status ) );///Mientras _Interrupt_Status esté en 0, significa que hay interrupciones pendientes
}

/**************************************************************************/
/**************************************************************************/
