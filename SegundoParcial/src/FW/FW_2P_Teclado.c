/** ==============================================
 *
 * @file		FW_2P_Teclado.c
 *
 *
 * @brief	
 *
 * @author		Mariano Koremblum,	marianotec7@gmail.com
 *
 * @date 		18/11/2013
 *
 * @version
 *
	============================================== */

/**************************************************************************/

//								INCLUDES

/**************************************************************************/

#include "SegundoParcial.h"

/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//						   GLOBAL VARIABLES

/**************************************************************************/

volatile short Bufferkey = NO_KEY;

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//								FUNCTIONS

/**************************************************************************/

/**
	\fn  void Teclado_Inicializacion(void)
	\brief Inicializa las GPIO para el teclado de la Expancion 3
 	\param void
 	\return void
 */
void Teclado_Inicializacion(void)
{
	SetPINSEL ( EXPANSION7 , FUNCION_GPIO);
	SetPINSEL ( EXPANSION8 , FUNCION_GPIO);
	SetPINSEL ( EXPANSION9 , FUNCION_GPIO);
	SetPINSEL ( EXPANSION10 , FUNCION_GPIO);
	SetPINSEL ( EXPANSION11 , FUNCION_GPIO);
	SetPINSEL ( EXPANSION12 , FUNCION_GPIO);

	//filas
	SetDIR ( EXPANSION7 , ENTRADA);
	SetDIR ( EXPANSION8 , ENTRADA);
	SetDIR ( EXPANSION9 , ENTRADA);
	SetDIR ( EXPANSION10 , ENTRADA);

	//columnas
	SetDIR ( EXPANSION11 , SALIDA);
	SetDIR ( EXPANSION12 , SALIDA);
}

/**************************************************************************/

/**
	\fn void Teclado_Debounce(void)
	\brief Filtra el rebote de una tecla. Cuando la tecla fue filtrada guarda su código en el buffer
 	\param unsigned char
 	\return void
 */
void Teclado_Debounce(unsigned char ActualKey)
{
	static uint8_t matches = 0;
	static uint8_t delayer = 0;
	static unsigned char PreviousKey = NO_KEY;

	/**
	 * La variable delayer se utiliza para dejar un espacio temporal entre la validación de una tecla y la siguiente.
	 * (Para que no suceda que tome dos veces la misma tecla si solo se pulso una vez)
	 */
	if(delayer)	delayer--;

	else
	{
		///Si la muestra actual es distinta a la anterior entonces la descarto
		if(ActualKey!=PreviousKey)
		{
			matches = 0;
			PreviousKey = ActualKey;///Igualo las muestras
			return;
		}

		///Filtra los casos en que la tecla actual es "NO_KEY"
		if(ActualKey!=NO_KEY)
		{
			///Suma la variable que cuenta las "coincidencias" de muestras
			matches++;

			/**
			 * Si se alcanzó un número de coincidencias empírico entonces
			 * se setea la tecla y el delay, y se reinicia matches
			 * */
			if(matches > MIN_MATCHES)
			{
				Bufferkey = ActualKey;
				PreviousKey = NO_KEY;
				delayer = DELAY_TIMES;
				matches = 0;
			}
		}
	}
}

/**************************************************************************/

/**
	\fn char Teclado_Lectura(void)
	\brief barre y lee la configuración matricial de teclado de la Expansión 3
 	\param void
 	\return codigo de tecla por éxito o NO_KEY si no hay ninguna tecla pulsada
 */
unsigned char Teclado_Lectura(void)
{
	{
		SET_COLUMNA1_ON;
		SET_COLUMNA2_OFF;

		if(FILA1_OFF)
			return SW4;

		if(FILA2_OFF)
			return SW1;

		if(FILA3_OFF)
			return SW3;

		if(FILA4_OFF)
			return SW2;

		SET_COLUMNA1_OFF;
	}

	{
		SET_COLUMNA2_ON;
		SET_COLUMNA1_OFF;

		if(FILA1_OFF)
			return SW5;

		if(FILA2_OFF)
			return SW8;

		if(FILA3_OFF)
			return SW6;

		if(FILA4_OFF)
			return SW7;

		SET_COLUMNA2_OFF;
	}

	return NO_KEY;
}

/**************************************************************************/
/**************************************************************************/
