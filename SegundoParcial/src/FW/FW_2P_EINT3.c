/** ==============================================
 *
 * @file		FW_2P_EINT3.c
 *
 *
 * @brief	
 *
 * @author		Mariano Koremblum,	marianotec7@gmail.com
 *
 * @date 		18/11/2013
 *
 * @version		v1.0
 *
	============================================== */

/**************************************************************************/

//							INCLUDES

/**************************************************************************/

#include "SegundoParcial.h"

/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//					   GLOBAL VARIABLES

/**************************************************************************/

volatile bool EmergencyButton;

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//							FUNCTIONS

/**************************************************************************/

/**
	\fn  void EINT3_Inicializacion(void)
	\brief Inicializa La interrupción externa 3 como entrada de emergencia
 	\param void
 	\return void
 */
void EINT3_Inicializacion(void)
{
	SetPINSEL( 2, 13, FUNCION_1);				//Configuro el P213 como interrup Ext EINT 3

	EXTMODE=(0x0F);					// Todas x Flanco
	EXTPOLAR=(0x00);				//Todas Polaridad activo bajo

	ISER0 |=(0x01 <<21);            // Habilito Interrupcion externa 3
}

/**************************************************************************/

/**
	\fn  EINT3_IRQHandler(void)
	\brief IRQHandler de la interrupción externa 3
 	\param void
 	\return void
 */
void EINT3_IRQHandler(void)
{
	EXTINT = EXTINT | (0x01<<EINT3);

	EmergencyButton = 1;

	///todo: Completar con lo que se requiera hacer para dicha interrupción
}

/**************************************************************************/
/**************************************************************************/
