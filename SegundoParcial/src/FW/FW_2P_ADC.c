/** ==============================================
 *
 * @file		FW_2P_ADC.c
 *
 * @author		Mariano Koremblum,	marianotec7@gmail.com
 *
 * @date 		18/11/2013
 *
	============================================== */

/**************************************************************************/

//								INCLUDES

/**************************************************************************/

#include "SegundoParcial.h"

/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//						   GLOBAL VARIABLES

/**************************************************************************/

volatile uint32_t BufferADC[MAX_ADC]; // Buffer ADC

volatile uint8_t ADC_inx_in; // Buffer Índice de entrada del buffer de ADC

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//								FUNCTIONS

/**************************************************************************/

/**
	\fn  void ADC_Inicializacion(void)
	\brief Inicializa El canal 5 del ADC para que trabaje en Free Run y avise el fin de conversión por interrupciones
 	\param void
 	\return void
 */
void ADC_Inicializacion(void)
{
	//1.- Prendo el ADC (Periferico)
	PCONP->PCADC = ON;

	//2.- Seteo el clock del ADC a 25MHz:
	PCLKSEL->PCLK_ADC = 0;

	//3.- Seteo el divisor en 1 para que muestree a 200kHz:
	_ADC->_CLKDIV = ON;

	//4.- Configuro los pines dependiendo del ADC que deseo utilizar!!!
	SetPINSEL(ADC5,FUNCION_3);
	SetDIR(ADC5,ENTRADA);

	//5.- Deshabilito las interrupciones:
	_ADC->_ADINTEN0 = OFF;
	_ADC->_ADINTEN1 = OFF;
	_ADC->_ADINTEN2 = OFF;
	_ADC->_ADINTEN3 = OFF;
	_ADC->_ADINTEN4 = OFF;
	_ADC->_ADINTEN5 = OFF;
	_ADC->_ADINTEN6 = OFF;
	_ADC->_ADINTEN7 = OFF;

	_ADC->_ADGINTEN = OFF;

	//6.- Elijo el/los ADC de los que voy a Muestrear:
	_ADC->_SEL |= (1<<5);  /// EJ: 00000010 (elijo el AD0.1)

	//7.- Activo el ADC:
	_ADC->_PDN = ON;

	//8.- Selecciono que el ADC muestree solo e inicializo el timer en 0:
	_ADC->_START = OFF;
	_ADC->_EDGE = OFF;
	_ADC->_BURST = ON;

	///Habilito la interrupción del Canal 5
	_ADC->_ADINTEN5 = ON;

	///Habilito la interrupción en el NVIC
	ISER0 |= (1<<22);
}

/**************************************************************************/

/**
	\fn  void ADC_IRQHandler(void))
	\brief IRQHandler del ADC
 	\param void
 	\return void
 */
void ADC_IRQHandler(void)
{
	///Cargo el valor del ADC en un buffer recursivo
	BufferADC[ADC_inx_in % MAX_ADC] = _ADC->_AD5_RESULT;

	ADC_inx_in++;
}

/**************************************************************************/
/**************************************************************************/
