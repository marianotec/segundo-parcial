/** ==============================================
 *
 * @file		FW_2P_TC.c
 *
 *
 * @brief	
 *
 * @author		Mariano Koremblum,	marianotec7@gmail.com
 *
 * @date 		18/11/2013
 *
 * @version		v1.0
 *
	============================================== */

/**************************************************************************/

//								INCLUDES

/**************************************************************************/

#include "SegundoParcial.h"

/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//						   GLOBAL VARIABLES

/**************************************************************************/

volatile uint32_t BufferTimers[ MAX_TIMERS ];

volatile uint8_t BufferEventos[ MAX_TIMERS ];

volatile bool fTimer0 ;

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//							FUNCTIONS

/**************************************************************************/

// !!!!!!!!!!!!!!!! PREGUNTAR TIEMPO DE INTERRUPCION
/**
	\fn  void TIMER0_Inicializacion(void)
	\brief Inicializa el Timer 0 como contador e interrupción por match
 	\param void
 	\return void
 */
void TIMER0_Inicializacion(void)
{
	PCONP->PCTIM0 = ON;
	PCLKSEL->PCLK_TIMER0 = 1;
	*ISER = 2;

	TIMER0->_IR = OFF;
	TIMER0->_COUNTER_ENABLE = OFF;

	//	/*
	//	 * Cuenta cada 1 useg ya que frec_uC = 100MHZ y PCLK_TM0 = 1/4 frec_uC = 25MHZ y preescaler = 25
	//	 * => frec_Timer = 1 MHZ (suma 1 cada 1 usec)
	//	 */
	//	TIMER0->_PR = 25000;

	TIMER0->_COUNTER_RESET = ON;
	TIMER0->_COUNTER_RESET = OFF;

	///Seteo del tipo de Modo Timer/Counter
	TIMER0->_C_T_MODE = COUNTER_CAPTURE_EDGE; //todo: Saber que valor ponerle a la constante (Que tipo de flanco se desea capturar)

	//Interrupciones de captura (RE: Rising Edge  --- FE: Falling Edge --- I: Interrupt)
	TIMER0->_CAP0RE = OFF;
	TIMER0->_CAP0FE = OFF;
	TIMER0->_CAP0I = OFF;

	TIMER0->_CAP1RE = OFF;
	TIMER0->_CAP1FE = OFF;
	TIMER0->_CAP1I = OFF;

	///todo: inicializar los pines de captura en dicho modo

	SetPINSEL(ED0,FUNCION_3);

	///Seteo del valor de Match
	TIMER0->_MR0 = INIT_VALUE_MR0;
	///Habilita la interrupción por Match
	TIMER0->_MR0I = ON;
	///Al hacer un Match, se reinicia la cuenta
	TIMER0->_MR0R = ON;

	TIMER0->_COUNTER_ENABLE = ON;
}

/**************************************************************************/

/**
	\fn void TIMER0_IRQHandler(void)
	\brief IRQHandler del TIMER0/COUNTER0. Pone en uno a la variable fTimer0 indicando que se genero la interpción
 	\param void
 	\return void
 */
void TIMER0_IRQHandler(void)
{
	TIMER0->_MR0_INTERRUPT = 1;

	fTimer0 = 1;
}


/**************************************************************************/
/**************************************************************************/
