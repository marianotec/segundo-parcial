/** ==============================================
 *
 * @file		FW_2P_Systick.c
 *
 *
 * @brief	
 *
 * @author		Mariano Koremblum,	marianotec7@gmail.com
 *
 * @date 		18/11/2013
 *
 * @version
 *
	============================================== */

/**************************************************************************/

//								INCLUDES

/**************************************************************************/

#include "SegundoParcial.h"

/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//						   GLOBAL VARIABLES

/**************************************************************************/

volatile uint32_t ticks;
volatile bool MaquinaOn;

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//								FUNCTIONS

/**************************************************************************/

/**
	\fn  void SysTick_Inicializacion(void)
	\brief Inicializa el SysTick para interrumpir cada 2,5 ms
 	\param void
 	\return void
 */
void SysTick_Inicializacion(void)
{
	systick->_STRELOAD  = (TENMS/4)-1; //(10ms)/4 -1 == 2,5ms <---- Interrumpe cada 2,5mseg
	systick->_STCURR = OFF;

	systick->_ENABLE = ON;
	systick->_TICKINT = ON;
	systick->_CLKSOURCE = ON;
}

/**************************************************************************/

/**
	\fn void SysTick_Handler(void)
	\brief IRQHandler del SysTick
 	\param void
 	\return void
 */
void SysTick_Handler(void)
{
	ticks ++;

	GET_KEY;

	Display_Barrrido();

	CheckStart();

	Relays();

	RGB_Refresh();

	if(ONE_SECOND_ELAPSED)
		TimerStatus();

	///todo: Agregar las funciones que falten al scheduler
}

/**************************************************************************/
/**************************************************************************/
