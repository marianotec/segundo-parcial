/** ==============================================
 *
 * @file		FW_2P_IO.c
 *
 *
 * @brief	
 *
 * @author		Mariano Koremblum,	marianotec7@gmail.com
 *
 * @date 		18/11/2013
 *
 * @version		v1.0
 *
	============================================== */

/**************************************************************************/

//								INCLUDES

/**************************************************************************/

#include "SegundoParcial.h"

/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//						   GLOBAL VARIABLES

/**************************************************************************/

volatile bool BufferRGB[3];
volatile uint8_t BufferRelays[4]; //todo poner como bool

/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//								FUNCTIONS

/**************************************************************************/

/**
	\fn  void IO_Inicializacion(void)
	\brief Inicializa las GPIO que se utilzaran para la automacion y señalizacion del proyecto
 	\param void
 	\return void
 */
void IO_Inicializacion(void)
{
	Inicializar_Relays();

	Inicializar_EDx();

	Inicializar_RGB();

	Inicializar_Buzzer();

}

/**************************************************************************/

void Inicializar_EDx(void)
{
	SetPINSEL(ED1, FUNCION_GPIO);
	SetPINSEL(ED2, FUNCION_GPIO);

	SetDIR(ED1, ENTRADA);
	SetDIR(ED2, ENTRADA);

	SetPINMODE(ED1,PINMODE_PULLUP);
	SetPINMODE(ED2,PINMODE_PULLUP);
}

/**************************************************************************/

///Función que inicializa el RGB
void Inicializar_RGB( void )
{
	SetPINSEL ( RGBR , FUNCION_GPIO);
	SetPINSEL ( RGBG , FUNCION_GPIO);
	SetPINSEL ( RGBB , FUNCION_GPIO);

	SetDIR ( RGBR , SALIDA);
	SetDIR ( RGBG , SALIDA);
	SetDIR ( RGBB , SALIDA);

	SetPIN ( RGBR , ACTIVO_BAJO);
	SetPIN ( RGBG , ACTIVO_BAJO);
	SetPIN ( RGBB , ACTIVO_BAJO);
}

/**************************************************************************/

///Función que inicializa los Relays
void Inicializar_Relays( void )
{
	SetPINSEL ( RELAY1 , FUNCION_GPIO);
	SetPINSEL ( RELAY2 , FUNCION_GPIO);
	SetPINSEL ( RELAY3 , FUNCION_GPIO);
	SetPINSEL ( RELAY4 , FUNCION_GPIO);

	SetDIR ( RELAY1 , SALIDA);
	SetDIR ( RELAY2 , SALIDA);
	SetDIR ( RELAY3 , SALIDA);
	SetDIR ( RELAY4 , SALIDA);
}

/**************************************************************************/

void Inicializar_Buzzer(void)
{
	SetPINSEL ( BUZZ , FUNCION_GPIO);

	SetDIR ( BUZZ , SALIDA);

	SetPIN ( BUZZ, ACTIVO_BAJO);
}

/**************************************************************************/

void Relays(void)
{
	SetPIN(RELAY1,BufferRelays[0]);

	SetPIN(RELAY2,BufferRelays[1]);

	SetPIN(RELAY3,BufferRelays[2]);

	//TUVE PROBLEMAS DE HARDWARE... solo de esta forma toma realmente el valor que deseo
	GPIO[ 5 ] &= ( ~ ( 1 << 27 ) );
	GPIO[ 5 ] |= ( BufferRelays[3] << 27 );
	GPIO[ 5 ] &= ( ~ ( 1 << 27 ) );
	GPIO[ 5 ] |= ( BufferRelays[3] << 27 );

	return;
}

/**************************************************************************/

void RGB_Refresh(void)
{
	static uint8_t state = 0;

	state++;

	if((state%2) == 0)
	{
		SetPIN(RGBR,BufferRGB[0]);
		SetPIN(RGBG,BufferRGB[1]);
		SetPIN(RGBB,BufferRGB[2]);

		return;
	}

	RGB_OFF;
}

/**************************************************************************/
/**************************************************************************/
