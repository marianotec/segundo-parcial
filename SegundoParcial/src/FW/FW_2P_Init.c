/** ==============================================
 *
 * @file		FW_2P_Init.c
 *
 *
 * @brief	
 *
 * @author		Mariano Koremblum,	marianotec7@gmail.com
 *
 * @date 		19/11/2013
 *
 * @version		v1.0
 *
	============================================== */

/**************************************************************************/

//								INCLUDES

/**************************************************************************/

#include "SegundoParcial.h"

/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//						   GLOBAL VARIABLES

/**************************************************************************/



/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//								FUNCTIONS

/**************************************************************************/

///Función que integra las inicializaciones de todos los periféricos
void Init(void)
{
	InitPLL();

	ADC_Inicializacion();

	IO_Inicializacion();

	Display_Inicializacion();

	EINT3_Inicializacion();

	TIMER0_Inicializacion();

	Teclado_Inicializacion();

	UART0_Inicializacion();

	SysTick_Inicializacion();

	Buffers_Inicializacion();

	///todo: Asegurarse que no falte agregar ningun inicializador!!
}

/**************************************************************************/

/**
 * Inicializa los buffers globales
 */
void Buffers_Inicializacion(void)
{
	uint8_t index;

	for(index = 0; index < MAX_TIMERS; index++)
	{
		BufferTimers[index]=0;
		BufferEventos[index]=0;
	}

	for(index = 0; index < DIGITOS; index++)
		BufferDisplay[index] = 0xF;

	for(index = 0; index < 3; index++)
		BufferRGB[index]=0;
}

/**************************************************************************/
/**************************************************************************/
