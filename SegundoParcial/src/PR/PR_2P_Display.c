/** ==============================================
 *
 * @file		PR_2P_Display.c
 *
 *
 * @brief	
 *
 * @author		Mariano Koremblum,	marianotec7@gmail.com
 *
 * @date 		18/11/2013
 *
 * @version
 *
	============================================== */

/**************************************************************************/

//							INCLUDES

/**************************************************************************/

#include "SegundoParcial.h"

/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//						   GLOBAL VARIABLES

/**************************************************************************/



/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//							FUNCTIONS

/**************************************************************************/

/**
	\fn  void Display(int Val,char dsp)
	\brief Presentación de valores en el display de 3 dígitos
 	\param [in] val valor a mostrar
 	\param [in] dsp selección del sector del display
	\return void
 */
void Display(uint16_t Val, uint8_t dsp)
{
	if(Val > MAX_DISPLAY_VALUE)
		return;

	switch(dsp)
	{
	case DISPLAY_LEFT:
		BufferDisplay[0]=CENTENA(Val);
		BufferDisplay[1]=DECENA(Val);
		BufferDisplay[2]=UNIDAD(Val);
		break;
	case DISPLAY_RIGHT:
		BufferDisplay[3]=CENTENA(Val);
		BufferDisplay[4]=DECENA(Val);
		BufferDisplay[5]=UNIDAD(Val);
		break;
	}
}

/**************************************************************************/

void SetDisplayDigit(uint8_t Val, uint8_t digit)
{
	if(digit < DIGITOS)
		BufferDisplay[digit]=Val;
}

/**************************************************************************/
/**************************************************************************/
