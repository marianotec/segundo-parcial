/** ==============================================
 *
 * @file		PR_2P_UART.c
 *
 *
 * @brief	
 *
 * @author		Mariano Koremblum,	marianotec7@gmail.com
 *
 * @date 		20/11/2013
 *
 * @version
 *
	============================================== */

/********************************************************************************************************/

//										INCLUDES								

/********************************************************************************************************/

#include "SegundoParcial.h"

/********************************************************************************************************/
/********************************************************************************************************/


/********************************************************************************************************/

//								   GLOBAL VARIABLES								

/********************************************************************************************************/



/********************************************************************************************************/
/********************************************************************************************************/

/********************************************************************************************************/

//										FUNCTIONS								

/********************************************************************************************************/

void PushRX (uint8_t dato)
{
	BuffRX0 [ IndiceBuff_InRX0 ] = dato;
	IndiceBuff_InRX0++;
	IndiceBuff_InRX0 %= MAX_RX0;
	return;
}

/**************************************************************************/

int PopRX (void)
{
	int dato = INVALID_CHARACTER;

	if(IndiceBuff_OutRX0 != IndiceBuff_InRX0)
	{
		dato = (int)BuffRX0 [ IndiceBuff_OutRX0 ];
		IndiceBuff_OutRX0++;
		IndiceBuff_OutRX0 %= MAX_RX0;
	}

	return dato;
}

/**************************************************************************/

uint8_t PushTx(uint8_t dato)
{
	if(tx_buffer_full)
		return 1;	//buffer lleno

	BufferTx[TX_inx_in] = dato;

	TX_inx_in++;
	TX_inx_in %= MAX_TX;

	tx_buffer_empty = 0;	//si agrego un dato el buffer ya no esta vacio

	if(TX_inx_in == TX_inx_out)
		tx_buffer_full = 1;	//se lleno el buffer

	if (txStart == 0)
	{
		txStart = 1;

		PopTx(&dato);

		if(dato < 10)
			dato+=48;

		UART0->_THR = dato;

		//START_TX(dato);
	}

	return 0;	//dato agregado al buffer
}

/**************************************************************************/

uint8_t PopTx( uint8_t *dato )
{
	if(tx_buffer_empty)
		return 1;	//buffer vacio

	*dato = (uint8_t) BufferTx[TX_inx_out];
	TX_inx_out++;
	TX_inx_out %= MAX_TX;
	tx_buffer_full = 0;	//si saco un dato el buffer ya no esta lleno

	if(TX_inx_out == TX_inx_in)
		tx_buffer_empty = 1;	//se vacio el buffer

	return 0;	//dato sacado del buffer
}


/**************************************************************************/
// suponemos que la trama arranca con "#" y termina con "$", en el medio se concatenan los datos

void GetMessage (void)
{
	static uint8_t WeftState = WAIT_STRING;
	static uint8_t WeftIndex = 0;
	static char aux[MAX_MESSAGE_SIZE];
	int character;

	character = PopRX();

	if(NO_CHARACTER)
		return;

	switch (WeftState)
	{
	case WAIT_STRING:
		if (TRAMA_START)
		{
			WeftIndex = 0;

			WeftState = GET_STRING;
		}
		break;

	case GET_STRING:
		if (TRAMA_START)
			WeftIndex = 0;

		else
		{
			if (!TRAMA_END && WeftIndex < (MAX_MESSAGE_SIZE-1))//MODIFIQUE ESTA LINEA!!! PROBAR!!!
			{
				aux[WeftIndex] = character;

				WeftIndex++;
			}

			else if(TRAMA_END)
			{
				aux[WeftIndex] = '\0';

				strcpy((char *)Message, aux);

				WeftState = WAIT_STRING;
			}

			else
				WeftState = WAIT_STRING;
		}
		break;

	default:
		WeftState = WAIT_STRING;
		break;
	}
}

/********************************************************************************************************/
/********************************************************************************************************/
