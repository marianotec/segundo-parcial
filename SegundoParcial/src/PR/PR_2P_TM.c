/** ==============================================
 *
 * @file		PR_2P_TM.c
 *
 *
 * @brief	
 *
 * @author		Mariano Koremblum,	marianotec7@gmail.com
 *
 * @date 		18/11/2013
 *
 * @version
 *
	============================================== */

/**************************************************************************/

//								INCLUDES

/**************************************************************************/

#include "SegundoParcial.h"

/**************************************************************************/
/**************************************************************************/


/**************************************************************************/

//						   GLOBAL VARIABLES

/**************************************************************************/



/**************************************************************************/
/**************************************************************************/

/**************************************************************************/

//								FUNCTIONS

/**************************************************************************/

/**
	\fn  void TimerStart ( uint8_t ev , uint8_t t )
	\brief Dispara un temporizador de base de tiempo de un segundo
 	\param [in] ev numero de timer
 	\param [in] t tiempo en segundos
	\return void
 */
void TimerStart ( uint8_t ev , uint8_t t )
{
	if(t)
	{
		BufferTimers[ev] = t;
		BufferEventos[ev] = 0;
	}
}

/**************************************************************************/

/**
	\fn  void TimerStop ( uint8_t ev )
	\brief Detiene un temporizador
 	\param [in] ev número de timer
	\return void
 */
void TimerStop ( uint8_t ev )
{
	BufferTimers[ev] = 0;
	BufferEventos[ev] = 0;
}

/**************************************************************************/

/**
	\fn  void TimerClose ( void )
	\brief Detiene todos los temporizadores
	\return void
 */
void TimerClose ( void )
{
	uint8_t index;

	for(index = 0; index < MAX_TIMERS; index++)
	{
		BufferTimers[index] = 0;
		BufferEventos[index] = 0;
	}
}

/**************************************************************************/

/**
 	\fn  void TimerStatus( void )
	\brief Verifica el estado de los timers
	\return void
 */
void TimerStatus(void)
{
	uint8_t index = 0;

	for(index = 0; index < MAX_TIMERS; index++)
	{
		if(BufferTimers[index] > 0)
		{
			BufferTimers[index]--;

			if(!BufferTimers[index])
				BufferEventos[index] = 1;
		}
	}
}

/**************************************************************************/
/**************************************************************************/
