/** ==============================================
 *
 * @file		PR_2P_EnClase.c
 *
 *
 * @brief	
 *
 * @author		Mariano Koremblum,	marianotec7@gmail.com
 *
 * @date 		22/11/2013
 *
 * @version
 *
	============================================== */

/********************************************************************************************************/

//										INCLUDES								

/********************************************************************************************************/

#include "SegundoParcial.h"

/********************************************************************************************************/
/********************************************************************************************************/


/********************************************************************************************************/

//								   GLOBAL VARIABLES								

/********************************************************************************************************/

volatile uint16_t DeltaT = 5;
volatile uint16_t SetPoint = 25;
volatile float ValorMedido = 0;

/********************************************************************************************************/
/********************************************************************************************************/

/********************************************************************************************************/

//										FUNCTIONS								

/**************************************************************************/

//Funcion que contiene a todas las demas funciones necesarias para la ejecucion de la aplicacion
void Control (void)
{
	if(MaquinaOn) Lazo_Control(); //Maquina de Control

	Calefaccion(); //Maquina que maneja el calor

	Semaforo(); //Maquina que maneja el led RGB

	SendTemperature(); //Funcion que envia la temperatura por UART

	ShowTemperature(); //Funcion que muestra la temperatura en el display

	ShowSetPoint(); //Funcion que muestra el setpoint de temperatura en el display

	ConfigSetPoint(); //Funcion que cambia los valores del setpoint segun toquen teclas
}

/**************************************************************************/

//Funcion que se encarga de enviar la temperatura cada cierto tiempo
void SendTemperature(void)
{
	static bool send = 0;

	if(!send)
	{
		TimerStart( 6 , 10 ); //Se pidió que se envie cada 10 segundos

		send = 1;
	}

	else if(BufferEventos[6]) //Si vence el timer, comienzo el envío
	{
		BufferEventos[6]=0;

		EnviarTemp();

		send = 0;
	}
}

/**************************************************************************/

//Funcion que constituye la Máquina de Estados del Lazo de Control
void Lazo_Control (void)
{
	static uint8_t Estado = INICIO;

	switch(Estado)
	{
	case INICIO:
		Estado = Lazo_inicio();
		break;

	case ESTADO1:
		Estado = Estado_Uno();
		break;

	case ESTADO2:
		Estado = Estado_Dos();
		break;

	case ESTADO3:
		Estado = Estado_Tres();
		break;

	case ESTADO4:
		Estado = Estado_Cuatro();
		break;

	case SAMPLE:
		Estado = Sample();
		break;

	case DEFAULT:
		MaquinaOn = 0;
		Lazo_inicio();
		Estado = INICIO;
		break;
	}

	if(EmergencyButton)
	{
		EmergencyButton = 0;

		Estado = DEFAULT;
	}
}

/**************************************************************************/

//Funcion de estado de la Máquina de Control
uint8_t Lazo_inicio (void)
{
	SET_RELAY0_OFF;
	SET_RELAY1_ON;
	SET_RELAY2_OFF;

	TimerStart(0,T0);

	return ESTADO1;
}

/**************************************************************************/

//Funcion de estado de la Máquina de Control
uint8_t Estado_Uno (void)
{
	if(BufferEventos[0])
	{
		SET_RELAY0_ON;

		TimerStart(0,T1);

		return ESTADO2;
	}

	return ESTADO1;
}

/**************************************************************************/

//Funcion de estado de la Máquina de Control
uint8_t Estado_Dos (void)
{
	if(BufferEventos[0])
	{
		SET_RELAY0_OFF;
		SET_RELAY2_ON;

		TimerStart(0,T2);
		TimerStart(2,T4);

		return ESTADO3;
	}

	return ESTADO2;
}

/**************************************************************************/

//Funcion de estado de la Máquina de Control
uint8_t Estado_Tres (void)
{
	if(BufferEventos[0])
	{
		BufferEventos[0] = 0;

		SET_RELAY0_ON;

		TimerStart(1,T3);
	}

	else if(BufferEventos[1])
	{
		SET_RELAY0_OFF;
		SET_RELAY1_OFF;

		BufferEventos[1]=0;
	}

	else if(BufferEventos[2])
	{
		SET_RELAY2_OFF;

		TimerStart(2,T5);

		return ESTADO4;
	}

	return ESTADO3;
}

/**************************************************************************/

//Funcion de estado de la Máquina de Control
uint8_t Estado_Cuatro(void)
{
	if(BufferEventos[2])
	{
		SET_RELAY2_ON;

		TIMER0->_COUNTER_RESET = 1;
		TIMER0->_COUNTER_RESET = 0;

		fTimer0 = 0;

		return SAMPLE;
	}

	return ESTADO4;
}

/**************************************************************************/

//Funcion de estado de la Máquina de Control
uint8_t Sample (void)
{
	if(fTimer0)
	{
		fTimer0 = 0;

		return INICIO;
	}

	return SAMPLE;
}

/**************************************************************************/
/**************************************************************************/

//Funcion que constituye la Máquina de estados del semáforo
void Semaforo (void)
{
	static uint8_t Estado = ROJO;

	switch(Estado)
	{
	case ROJO:
		Estado = Sem_Rojo();
		break;

	case VERDE:
		Estado = Sem_Verde();
		break;

	case AZUL:
		Estado = Sem_Azul();
		break;

	default:
		Estado = ROJO;
		break;
	}
}

/**************************************************************************/

//Funcion de estado de la Máquina de semaforos
uint8_t Sem_Rojo(void)
{
	static bool index = 0;

	if(!index)
	{
		SET_AZUL_OFF;
		SET_ROJO_ON;

		index = 1;
	}

	if(ValorMedido <= SetPoint)
	{
		index = 0;

		return AZUL;
	}

	parpadeo();

	return ROJO;
}

/**************************************************************************/

//Funcion de estado de la Máquina de semaforos
uint8_t Sem_Azul(void)
{
	static bool index = 0;

	if(!index)
	{
		SET_ROJO_OFF;
		SET_VERDE_OFF;
		SET_AZUL_ON;

		index = 1;
	}

	if(ValorMedido <= SetPoint-DeltaT)
	{
		index = 0;

		return VERDE;
	}

	if(ValorMedido > SetPoint)
	{
		index = 0;

		return ROJO;
	}

	return AZUL;
}

/**************************************************************************/

//Funcion de estado de la Máquina de semaforos
uint8_t Sem_Verde(void)
{
	static bool index = 0;

	if(!index)
	{
		SET_AZUL_OFF;
		SET_VERDE_ON;

		index = 1;
	}

	if(ValorMedido > SetPoint-DeltaT)
	{
		index = 0;

		return AZUL;
	}
	return VERDE;
}

/**************************************************************************/

//Funcion que hace parpadear al led rojo
void parpadeo (void)
{
	static bool index = 0;

	if(!index)
	{
		SET_ROJO_ON;

		TimerStart(4,2);

		index = 1;
	}

	else
	{
		if(BufferEventos[4])
		{
			BufferEventos[4]=0;

			SET_ROJO_OFF;

			TimerStart(5,2);
		}

		else if(BufferEventos[5])
		{
			BufferEventos[5] = 0;

			index = 0;
		}
	}
}

/**************************************************************************/
/**************************************************************************/

//Funcion que constituye la Máquina de estados del Calefactor
void Calefaccion (void)
{
	static uint8_t Estado = NO_CALENTAR;

	switch(Estado)
	{
	case NO_CALENTAR:
		Estado = Calentar_Off();
		break;

	case CALENTAR:
		Estado = Calentar();
		break;

	default:
		Estado = NO_CALENTAR;
		break;
	}
}

/**************************************************************************/

//Funcion de estado de la Máquina de calefaccion
uint8_t Calentar_Off(void)
{
	static bool index = 0;

	if(!index)
	{
		SET_RELAY3_OFF;

		index = 1;
	}

	ValorMedido = Temperatura (ADC());

	if(ValorMedido < SetPoint - DeltaT)
	{
		index = 0;

		return CALENTAR;
	}
	return NO_CALENTAR;
}

/**************************************************************************/

//Funcion de estado de la Máquina de calefaccion
uint8_t Calentar(void)
{
	static bool index = 0;

	if(!index)
	{
		SET_RELAY3_ON;

		index = 1;
	}

	ValorMedido = Temperatura (ADC());

	if(ValorMedido > SetPoint)
	{
		index = 0;

		return NO_CALENTAR;
	}
	return CALENTAR;
}

/**************************************************************************/

//La funcion retorna el valor de la temperatura, sacando una cuenta respecto de la entrada del ADC
float Temperatura (uint16_t cuentas){ return (float)((PENDIENTE*cuentas)-10); }

/**************************************************************************/

//Funcion que se encarga de cargar el buffer de transmision con la temperatura
void EnviarTemp (void)
{
	uint8_t index = 0;
	char string[8];//Posee 8 caracteres debido a que necesita el '\0'

	sprintf(string,"#%5.1f\n",ValorMedido); //Copia el valor de ValorMedido y lo convierte en formato string

	for(index = 0; index < 7 ; index++) //Pusheo los datos al buffer de Tx
		PushTx(string[index]);
}

/**************************************************************************/

//Funcion que muestra la temperatura en el display
void ShowTemperature (void){ Display(ValorMedido, 0); }

/**************************************************************************/

//Funcion que muestra el setpoint en el display
void ShowSetPoint (void){ Display(SetPoint, 1); }

/**************************************************************************/

//Funcion que aumenta o disminuye el valor del setpoint segun la tecla que fue pulsada
void ConfigSetPoint (void)
{
	uint8_t auxiliar = 0;

	auxiliar = Teclado();

	switch(auxiliar)
	{
	case SW1:
		SetPoint++;
		break;

	case SW2:
		SetPoint--;
		break;
	}
}

/**************************************************************************/

//Se fija el estado de una entrada digital, en caso que esté activada, se habilita el inicio de la máquina
void CheckStart (void)
{
	if(GetPIN(ED1,ACTIVO_BAJO))
		MaquinaOn = 1;
}

/**************************************************************************/


/**************************************************************************/
/**************************************************************************/
